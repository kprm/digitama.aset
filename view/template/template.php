<!DOCTYPE html> 
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Digitama Aset</title>

<link href="/public/css/bootstrap.min.css" rel="stylesheet">
<link href="/public/css/bootstrap-table.css" rel="stylesheet">
<link href="/public/css/datepicker3.css" rel="stylesheet">
<link href="/public/css/gaya.css" rel="stylesheet">
<link href="/public/css/styles.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/public/css/360player.css" />

<!--Icons-->
<script src="/public/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]> 
<script src="/public/js/html5shiv.js"></script>
<script src="/public/js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Digitama</span>User</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><!-- <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> -->
						<?php 
						$db =Db::init(); 
						$col = $db -> users;
						$data = $col -> findone (array("_id" => new mongoid($_SESSION['id']))); 
						$foto=$data['foto_rename']; 
						echo '<img src="/public/fotoUser/'.$foto.'" alt="User Avatar" class="img-circle" width="30" height="30" />' ;?><?php echo $_SESSION['nama']; ?>
						<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="/profil/ambildataProfil"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<!-- <li><a href="/profil/formSetting"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li> -->
							<li><a href="/login/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
				
				<ul class="user-menu">
					<li class="dropdown pull-right">
						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<svg class="glyph stroked two messages" ><use xlink:href="#stroked-two-messages" ></use></svg>Notifikasi <span class="load_row"></span></span>
							<!-- <ul class="dropdown-menu" role="menu"> -->
								<!-- <div id="info"><div id="loading"><br>Loading...</div><div id="konten-info"></div></div> -->
							<!-- </ul> -->
							<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">



							
									<span id="load_data"></span>
							</ul>
						</a>
						

					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<center></center><img src="/public/css/img/lg.png" alt="Avatar" class="img-circle" height="80" align="center" /></center> <br>
			<!--<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>-->
		</form>
		<ul class="nav menu">
			<?php 
				$directoryURI = $_SERVER['REQUEST_URI'];
				$path = parse_url($directoryURI, PHP_URL_PATH);
				$components = explode('/', $path);
				$first_part = $components[1];
			?>
			<li class="<?php if (($first_part=='') || ($first_part=='welcome'))  echo 'active'; ?>"><a href="/welcome/index"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Beranda</a></li>
			<!--<li><a href="widgets.html"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Kelola Aset</a></li>
			<li><a href="charts.html"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Charts</a></li>
			<li><a href="tables.html"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Tables</a></li>
			<li><a href="forms.html"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Forms</a></li>
			<li><a href="panels.html"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Alerts &amp; Panels</a></li>-->

			<li class="<?php if ($first_part=='myassets') echo 'active'; ?>"><a href="/myassets/coba"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> My Assets</a></li>
			<!-- <li class="<?php if ($first_part=='') echo 'active'; ?>"><a href="/myassets/myasset"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> My Assets</a></li>
			 -->
			 <li class="<?php if ($first_part=='group') echo 'active'; ?>"><a href="/project/listprojectuser"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Project </a></li>
			 <li class="<?php if ($first_part=='aset') echo 'active'; ?>"><a href="/aset/formAddAset"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Upload</a></li>
		</ul>

	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<!--<div class="row">
			<ol class="breadcrumb">
				<li><a href="/welcome/index"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row-->
		
		<?php echo $content; ?>
	</div>	<!--/.main-->

	<script src="/public/js/jquery-1.11.1.min.js"></script>
	<script src="/public/js/bootstrap.min.js"></script>
	<script src="/public/js/chart.min.js"></script>
	<script src="/public/js/chart-data.js"></script>
	<script src="/public/js/easypiechart.js"></script>
	<script src="/public/js/easypiechart-data.js"></script>
	<script src="/public/js/bootstrap-dateicker.js"></script>
	<script src="/public/js/bootstrap-table.js"></script>
	<script src="/public/js/bootbox.min.js"></script>
	<script src="/public/js/audio.min.js"></script>
	<script src="/public/js/notifikasi.js"></script>
	<script src="/public/js/dot.js"></script>
	<script type="text/javascript" src="/public/js/mp.mansory.js"></script>
	<script type="text/javascript" src="/public/js/berniecode-animator.js"></script>
	<script type="text/javascript" src="/public/js/soundmanager2.js"></script>
	<script type="text/javascript" src="/public/js/360player.js"></script>
	<script>
		setInterval(function(){
		$(".load_row").load('/notif/ceknotif')
		}, 2000); //menggunakan setinterval jumlah notifikasi akan selalu update setiap 2 detik diambil dari controller notifikasi fungsi load_row
		 

		setInterval(function(){
		$("#load_data").load('/notif/lihatnotif')
		}, 2000); //yang ini untuk selalu cek isi data notifikasinya sama setiap 2 detik diambil dari controller notifikasi fungsi load_data

	</script>
	<script type="text/javascript">
		jQuery(document).ready(function ( $ ) {
			$(".my-gallery-container").mpmansory(
				{
					childrenClass: 'item', // default is a div
					columnClasses: 'padding', //add classes to items
					breakpoints:{
						lg: 3, 
						md: 4, 
						sm: 6,
						xs: 12
					},
					distributeBy: { order: false, height: false, attr: 'data-order', attrOrder: 'asc' }, //default distribute by order, options => order: true/false, height: true/false, attr => 'data-order', attrOrder=> 'asc'/'desc'
					onload: function (items) {
						//make somthing with items
					} 
				}
			);
		});
	</script>
	<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-36251023-1']);
		  _gaq.push(['_setDomainName', 'jqueryscript.net']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

	</script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
	<script>
		audiojs.events.ready(function() {
		var as = audiojs.createAll();
		});
	</script>
	<script type="text/javascript">
	// <![CDATA[
	if (document.location.href.match(/debug=1/i)) {
	  document.getElementById('with-debug').style.display = 'none';
	}
	if (document.location.href.match(/debug/i)) {
	  document.getElementById('options-divider-with-html5').style.display = 'none';
	}
	if (soundManager.useHTML5Audio || document.location.href.match(/html5audio=1/i)) {
	  document.getElementById('with-html5').style.display = 'none';
	  document.getElementById('options-divider-with-html5').style.display = 'none';
	} else {
	  document.getElementById('without-html5').style.display = 'none';
	  document.getElementById('options-divider-without-html5').style.display = 'none';
	}
	init();
	// 	]]>
	</script>	
</body>

</html>
