<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login</title>

<link href="/public/css/bootstrap.min.css" rel="stylesheet">
<link href="/public/css/datepicker3.css" rel="stylesheet">
<link href="/public/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<style>
div.container {
    width: 35%;
    border: 0px solid white;
}

header, footer {
    padding: 1em;
    color: white;
    background-color: black;
    clear: left;
    text-align: center;
}
</style>


</head>

<body>

<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-body"><center><h3>Log in</h3></center></div>
				<div class="panel-body">
					<form role="form"  method="post" action="/login/index">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Masukkan E-mail" name="email" value="<?php echo $email; ?>" type="email" required autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Masukkan Password" name="password" type="password" value="" required="">
							</div>
							
							<center><input type="submit" name="submit" class="btn btn-primary" value="LOGIN"></center><br>
							<center><a href="<?php echo $urlfacebook; ?>" class="btn btn-primary">FACEBOOK</a> <a href="<?php echo $urlgoogle; ?>" class="btn btn-primary">GOOGLE</a></center>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
<!-- <div class="row">
	<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">Log in</div>
			<div class="panel-body" >
				<?php
				echo $message ;
				?>
				<form role="form" method="post" action="/login/index">
					<fieldset>
						
							<tr>
								<th colspan="3"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></th>
							</tr>
							<tr>
								<th height="40px"><b>Email</b></th>
								<th></th>
								<th><input class="form-control" placeholder="Masukkan E-mail" name="email" value="<?php echo $email; ?>" type="email" required></th>
							</tr>
							<tr>
								<th height="40px"><b>Password</b></th>
								<th></th>
								<th><input class="form-control" placeholder="Masukkan Password" name="password" type="password" value="" required=""></th>
							</tr>
							<tr>
								<td colspan="3" height="40px">
									<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
								</td>
							</tr>
							<tr>
								<th colspan="3" height="40px"><center><input type="submit" name="submit" class="btn btn-primary" value="LOGIN"></center></th>
							</tr>
							<tfoot>
								<tr>
									<th colspan="3" align="center" valign="center" style="height:40px;" ><center><a href="<?php echo $urlfacebook; ?>" class="btn btn-primary">FACEBOOK</a> <a href="<?php echo $urlgoogle; ?>" class="btn btn-primary">GOOGLE</a></center></th>
								</tr>
							</tfoot>
						</table>			
					</form>
				</div> -->


	<!--<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log In</div>
				<div class="panel-body" >
				<?php
				echo $message ;
				?>
					<form role="form" method="post" action="/login/index">
						<table width="100%" cellpadding="20px">
							<!--<tr>
								<th colspan="3"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></th>
							</tr>-->
							<!--<tr>
								<th height="40px"><b>Email</b></th>
								<th></th>
								<th><input class="form-control" placeholder="Masukkan E-mail" name="email" value="<?php echo $email; ?>" type="email" required></th>
							</tr>
							<tr>
								<th height="40px"><b>Password</b></th>
								<th></th>
								<th><input class="form-control" placeholder="Masukkan Password" name="password" type="password" value="" required=""></th>
							</tr>
							<tr>
								<td colspan="3" height="40px">
									<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
								</td>
							</tr>
							<tr>
								<th colspan="3" height="40px"><center><input type="submit" name="submit" class="btn btn-primary" value="LOGIN"></center></th>
							</tr>
							<tfoot>
								<tr>
									<th colspan="3" align="center" valign="center" style="height:40px;" ><center><a href="<?php echo $urlfacebook; ?>" class="btn btn-primary">FACEBOOK</a> </center></th>
								</tr>
								<tr>
									<th colspan="3" align="center" valign="center" style="height: 40px;"><center><a href="<?php echo $urlgoogle; ?>" class="btn btn-primary">GOOGLE</a></center></th>
								</tr>
							</tfoot>
						</table>			
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	<!--</div><!-- /.row -->	
	
		

	<script src="/public/js/jquery-1.11.1.min.js"></script>
	<script src="/public/js/bootstrap.min.js"></script>
	<script src="/public/js/chart.min.js"></script>
	<script src="/public/js/chart-data.js"></script>
	<script src="/public/js/easypiechart.js"></script>
	<script src="/public/js/easypiechart-data.js"></script>
	<script src="/public/js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>
</html>