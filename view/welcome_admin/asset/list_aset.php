	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Aset</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table data-toggle="table" data-url="/aset/ambildataAset"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc"> 
							<!-- <a href="/user/formAddUser"><button type="submit" class="btn btn-primary">Add User</button></a> -->
						    <thead>
						    <tr>
						        <!-- <th data-field="id" data-sortable="true" >Id User</th>-->
						        <th data-field="pemilik" data-sortable="true">Nama Pemilik</th> 
						        <th data-field="judul"  data-sortable="true">Judul</th>
						        <th data-field="tipe" data-sortable="true">Tipe</th>
						        <!-- <th data-field="group" data-sortable="true">Group/Project</th> -->
						        <th data-field="namaGrup" data-sortable="true">Nama Group/Project</th>
						        <th data-field="timeUpload"  data-sortable="true">Time Upload</th>
						        <th data-field="lastUpdate" data-sortable="true">Last Update</th>						        
						        <th data-field="desk" data-sortable="true">Deskripsi</th>
						        <th data-field="status" data-sortable="true">Status</th>
						        <th data-field="jmldownload" data-sortable="true">Jumlah Download</th> 
						        <th data-field="file_rename" data-formatter="getImage">Foto</th>
						        <th data-field="id" data-formatter="getPreview">Action</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		 
	<script type="text/javascript">
		 function getPreview(value, row) {
    		//hh = '<a href="/user/formEditUser?id='+value+'" >Edit</a><br />';	//tombol untuk aksi mengedit 
	    	hh = '<div class="label label-danger"><a href="#" class="hapusAset" dataid="'+value+'" >Hapus</a></div>';	//tombol untuk aksi menghapus 
	      	return hh;
    	}

    	function getImage(value,row){
    		var pisah = value.split("+");
		  	if(pisah[0]=="Picture"){
	    		str='<img src="/public/assets/pict/' +pisah[1]+'"  width="100" />' ;
	    		return str;	
    		} else if(pisah[0]=='Sketch'){
	    		str='<img src="/public/assets/sketch/' +pisah[1]+'"  width="100" />' ;
	    		return str;	
    		} else if(pisah[0]=='Audio'){
	    		str='<img src="/public/assets/audio/audio.png"  width="100" />' ;
	    		return str;	
    		} else if(pisah[0]=='3D'){
	    		str='<img src="/public/assets/3d/3d.png"  width="100" />' ;
	    		return str;	
    		}
    		  
    		 
    		
    		
    	}
	</script>


	
	</div><!--/.main-->