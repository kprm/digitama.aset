		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">All Picture</h1> 
			</div>
		</div><!--/.row-->

		
		<div class="" id="my-gallery-container">
			
			<?php 
				
				foreach ($data as $dt) {

					$db = Db::init();	
					$col = $db -> users;
					$data = $col -> findone (	//untuk memilih satu
						array(
							"_id" => new mongoid($dt['idUser'])
							)
						);

					$nama=$data['nama'];
					$foto=$data['foto_rename'];
					

					echo ' <div class="item"> 
			            <div class="thumbnail">
			                <div class="caption">
			                    <h4>'.$dt['judul'].'</h4>
			                    <p>'.$dt['desk'].'</p>
			                    <p><a href="/aset/detail?id='.$dt['_id'].'" class="label label-danger">View</a></p>
			                </div>
			                
			                <img class="img-responsive" src="/public/assets/pict/'.$dt['file_rename'].'" width="250">
			             </div>
			            </div>';
				
			} 
			?>
		</div>

		