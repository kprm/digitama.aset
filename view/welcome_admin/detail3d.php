<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Babylon - Basic scene</title>
    <style>
        html, body {
            overflow: hidden;
            width: 100%;
            height: 100%;
            margin-top: 30px;
            padding: 0;
        }
        #renderCanvas {
            width: 100%;
            height: 100%;
        }
    </style>
    <script src="/public/js/babylon.js"></script>
    <script src="/public/js/babylon.objFileLoader.js"></script>
</head>
<body>
<canvas id="renderCanvas"></canvas>

    <script type="text/javascript">

        // Get the canvas element from our HTML below
        var canvas = document.getElementById("renderCanvas");
        // Load the BABYLON 3D engine
        var engine = new BABYLON.Engine(canvas, true);
        var scene   = new BABYLON.Scene(engine);


        new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
        var cam = new BABYLON.ArcRotateCamera("ArcRotateCamera", 0, 0, 0, new BABYLON.Vector3(0, 3, 0), scene);
        cam.attachControl(canvas);

        var loader = new BABYLON.AssetsManager(scene);

        var position = 0;
        var pos = function(t) {
            t.loadedMeshes.forEach(function(m) {
                m.position.y -= position;
            });
            position += -20;
        };

        var mesh = loader.addMeshTask("plane", "", "/public/assets/3d/plane/", "A380.obj");
        mesh.onSuccess = pos;
        // var batman = loader.addMeshTask("batman", "", "Batman/", "Batman_Injustice.obj");
        // batman.onSuccess = pos;
        // var penguin = loader.addMeshTask("penguin", "", "Penguin/", "Penguin.obj");
        // penguin.onSuccess = pos;

        loader.onFinish = function() { 
            engine.runRenderLoop(function () {
                scene.render();
            });
        };

        loader.load();

        window.addEventListener("resize", function () {
            engine.resize();
        });

    </script>
</body>
</html>