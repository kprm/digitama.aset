	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Audit Trail</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table data-toggle="table" data-url="/audit/ambildata"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc"> 
							<!-- <a href="/user/formAddUser"><button type="submit" class="btn btn-primary">Add User</button></a> -->
						    <thead>
						    <tr>
						        <!-- <th data-field="id" data-sortable="true" >ID</th> -->
						        <!-- <th data-field="idUser" data-sortable="true">User</th>  -->
						        <th data-field="namaUser" data-sortable="true">Nama User</th> 
						        <th data-field="aksi"  data-sortable="true">Aksi</th>
						        <th data-field="tgl" data-sortable="true">Tanggal</th> 
						        <th data-field="jam"  data-sortable="true">Pukul</th>
						        <th data-field="ket" data-sortable="true">Keterangan</th>
						        <!-- <th data-field="group" data-sortable="true">Group/Project</th> -->
						       
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
	
	</div><!--/.main-->