		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Edit Group</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/group/updateGroup?id=<?php echo $id; ?>">
							
								<!-- Form untuk mengisi nama-->
								<?php
								$haserror_namaGrup = "";	//memberikan pesan error
								if(isset($error['namaGrup']))
						       	{
						       		$haserror_namaGrup = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_namaGrup; ?>">
									<label> Group / Project </label>
									<input class="form-control" name="namaGrup" value="<?php echo $namaGrup; ?>">
									<?php
									if(isset($error['namaGrup']))
							       	{
							           foreach($error['namaGrup'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>
								
								
								<!-- Form untuk mengisi tanggal dimulainya project-->
								<?php
								$haserror_tglMulai = "";	//memberikan pesan error
								if(isset($error['tglMulai']))
						       	{
						       		$haserror_tglMulai = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_tglMulai; ?>">
									<label>Tanggal Mulai </label>
									<table>
										<tr>
											<td><select class="form-control" name="tgl">
											<option value="<?php echo $tgl;?>"><?php echo $tgl;?></option>

											<?php
											for($tgl=1;$tgl<=31;$tgl++){

													echo '<option value="'.$tgl.'">'.$tgl.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulan">
													<option value="<?php echo $bulan;?>"><?php echo $bulan;?></option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahun">
											<option value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
											<?php

											$now = date('Y');
											$last = $now-150;
											for($thn=$now;$thn>=$last;$thn--){

													echo '<option value="'.$thn.'">'.$thn.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
									<?php
									if(isset($error['tglMulai']))
							       	{
							           foreach($error['tglMulai'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>
							

								<!-- Form untuk mengisi status-->
								<div class="form-group <?php echo $haserror_status; ?>">
									<label>Status </label>
										<select class="form-control" name="status">
											<option value="<?php echo $status;?>"><?php echo $status;?></option>
											<option value="Belum">Belum</option>
											<option value="Proses">Proses</option>
											<option value="Selesai">Selesai</option>
										</select>		
								</div>


								<!-- Form untuk mengisi keterangan-->
								
								<div class="form-group <?php echo $haserror_ket ?>">
									<label>Alamat</label>
									<textarea class="form-control" rows="3" name="ket" placeholder="Masukkan Keterangan"><?php echo $ket; ?></textarea>
								</div>

								<button type="submit" class="btn btn-primary"> Save </button>	<!-- button untuk menyimpan data--> 
								<button type="reset" class="btn btn-default"> Reset </button>	
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
		