	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Task List Project</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table data-toggle="table" data-url="/taskList/ambildata"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc"> 
							<a href="/taskList/formAddTask"><button type="submit" class="btn btn-primary">Add Group</button></a>
						    <thead>
						    <tr>
						        <th data-field="project" data-checkbox="true" >Project</th>
						        <th data-field="judul" data-sortable="true">Judul Aset</th>
						        <th data-field="deskripsi" data-sortable="true">Deskripsi </th>						    
						        <th data-field="dikerjakanOleh"  data-sortable="true">Dikerjakan Oleh</th>
						        <th data-field="dikerjakanPulaOleh" data-sortable="true">Dikerjakan Pula Oleh</th>
						        <th data-field="prioritas" data-sortable="true">Prioritas</th>					    
						        <th data-field="tglHarusSelesai"  data-sortable="true">Tanggal Haris Selesai</th>
						        <th data-field="status" data-sortable="true">Status</th>
						        <th data-field="ket" data-sortable="true">Keterangan</th>
						        <th data-field="id" data-formatter="getPreview">Action</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		
	<script type="text/javascript">
		 function getPreview(value, row) {
    		hh = '<div class="label label-primary"><a href="/taskList/formEditTask?id='+value+'" >Edit</a></div>';	//tombol untuk aksi mengedit 
	    	hh += '<div class="label label-danger"><a href="#" class="hapusTask" dataid="'+value+'">Hapus</a></div>';	//tombol untuk aksi menghapus 
	      	return hh;

    }
	</script>
	
	</div><!--/.main-->