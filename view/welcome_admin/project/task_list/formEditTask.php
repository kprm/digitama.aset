		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Edit Group</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/taskList/updateTask?id=<?php echo $id; ?>">
							
								
								<!-- Form untuk mengisi project-->
								<?php
								$haserror_project = "";	//memberikan pesan error
								if(isset($error['idProject']))
						       	{
						       		$haserror_project = "has-error";
						       	} 
								?>
								<div class="form-group <?php echo $haserror_project; ?>">
									<label> Project </label>
									<select name="idProject" class="form-control">
    									<option value="">-Pilih Project-</option>
    									<?php
								       	$db = Db::init();
										$col = $db -> project;
										$dt=$col->find();	// memilih semua == select all
										$data = array();
										$dta2 = $col->findone(array("_id" => $idProject));
										echo "<option value='".$idProject."'> ".$dta2['namaproject']." </option>";
										foreach($dt as $dta)
										{
					

										echo "<option value='".$dta['_id']."'> ".$dta['namaproject']." </option>";

										}
										?>
									</select>
									<?php
									if(isset($error['idProject']))
							       	{
							           foreach($error['idProject'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>

								<!-- Form untuk mengisi judul-->
								<?php
								$haserror_judul = "";	//memberikan pesan error
								if(isset($error['judul']))
						       	{
						       		$haserror_judul = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_judul; ?>">
									<label> Judul </label>
									<input class="form-control" name="judul" value="<?php echo $judul; ?>">
									<?php
									if(isset($error['judul']))
							       	{
							           foreach($error['judul'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi deskripsi-->
								<?php
								$haserror_deskripsi = "";	//memberikan pesan error
								if(isset($error['deskripsi']))
						       	{
						       		$haserror_deskripsi = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_deskripsi; ?>">
									<label>Deskripsi</label>
									<textarea class="form-control" rows="3" name="deskripsi"><?php echo $deskripsi; ?></textarea>
									<?php
									if(isset($error['deskripsi']))
							       	{
							           foreach($error['deskripsi'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>
								
								<!-- Form untuk mengisi dikerjakan oleh-->
								<?php
								$haserror_dikerjakanOleh = "";	//memberikan pesan error
								if(isset($error['dikerjakanOleh']))
						       	{
						       		$haserror_dikerjakanOleh = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_dikerjakanOleh; ?>">
									<label> Dikerjakan Oleh </label>
									<select name="dikerjakanOleh" class="form-control">
    									<?php
								       	$db = Db::init();
										$col = $db -> users;
										$dt=$col->find();	// memilih semua == select all
										$data = array();
										$dta2 = $col->findone(array("_id" => $dikerjakanOleh));
    									echo "<option value='".$dikerjakanOleh."'> ".$dta2['nama']."-".$dta2['bagian']." </option>";
										foreach($dt as $dta)
										{
					

										echo "<option value='".$dta['_id']."'> ".$dta['nama']."-".$dta['bagian']." </option>";

										}
										?>
									</select>
									<?php
									if(isset($error['dikerjakanOleh']))
							       	{
							           foreach($error['dikerjakanOleh'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi dikerjakan pula oleh-->
								<?php
								$haserror_dikerjakanPulaOleh = "";	//memberikan pesan error
								if(isset($error['dikerjakanPulaOleh']))
						       	{
						       		$haserror_dikerjakanPulaOleh = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_dikerjakanPulaOleh; ?>">
									<label> Dikerjakan Pula Oleh </label>
									<select name="dikerjakanPulaOleh" class="form-control">
    									<?php
								       	$db = Db::init();
										$col = $db -> users;
										$dt=$col->find();	// memilih semua == select all
										$data = array();
										$dta2 = $col->findone(array("_id" => $dikerjakanPulaOleh));
    									echo "<option value='".$dikerjakanPulaOleh."'> ".$dta2['nama']."-".$dta2['bagian']." </option>";
										foreach($dt as $dta)
										{
					

										echo "<option value='".$dta['_id']."'> ".$dta['nama']."-".$dta['bagian']." </option>";

										}
										?>
									</select>
									<?php
									if(isset($error['dikerjakanPulaOleh']))
							       	{
							           foreach($error['dikerjakanPulaOleh'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>


								<!-- Form untuk mengisi Prioritas-->
								<?php
								$haserror_prioritas = "";	//memberikan pesan error
								if(isset($error['prioritas']))
						       	{
						       		$haserror_prioritas = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_prioritas; ?>">
									<label>Prioritas</label>
									
									<select class="form-control" name="prioritas">
										<option value="<?php echo $prioritas; ?>"><?php echo $prioritas; ?></option>
										<option value="Low">Low</option>
										<option value="Middle">Middle</option>
										<option value="High">High</option>
										
									</select>
								
									<?php
									if(isset($error['prioritas']))
							       	{
							           foreach($error['prioritas'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>

								
								<!-- Form untuk mengisi tgl mulai-->
								<?php
								$haserror_tglHarusSelesai = "";	//memberikan pesan error
								if(isset($error['tglHarusSelesai']))
						       	{
						       		$haserror_tglHarusSelesai = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_tglHarusSelesai; ?>">
									<label>Tanggal Harus Selesai</label>
									<table>
										<tr>
											<td><select class="form-control" name="tglSelesai">
											<option value="<?php echo $tglSelesai; ?>"><?php echo $tglSelesai; ?></option>

											<?php
											for($tgl=1;$tgl<=31;$tgl++){

													echo '<option value="'.$tgl.'">'.$tgl.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulanSelesai">
													<option value="<?php echo $bulanSelesai; ?>"><?php echo $bulanSelesai; ?></option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahunSelesai">
											<option value="<?php echo $tahunSelesai; ?>"><?php echo $tahunSelesai; ?></option>
											<?php

											$now = date('Y');
											$last = $now+5;
											for($thn=$now;$thn<=$last;$thn++){

													echo '<option value="'.$thn.'">'.$thn.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
									<?php
									if(isset($error['tglHarusSelesai']))
							       	{
							           foreach($error['tglHarusSelesai'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>

								

								<!-- Form untuk mengisi status-->
								<div class="form-group <?php echo $haserror_status; ?>">
									<label>Status </label>
										<select class="form-control" name="status" value="<?php echo $status; ?>">
											<option value="<?php echo $status; ?>"><?php echo $status; ?></option>
											<option value="Belum">Belum</option>
											<option value="Proses">Proses</option>
											<option value="Selesai">Selesai</option>
										</select>		
											
								</div>

								<!-- Form untuk mengisi keterangan-->
								<?php
								$haserror_ket = "";	//memberikan pesan error
								if(isset($error['ket']))
						       	{
						       		$haserror_ket = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_ket ?>">
									<label>Alamat</label>
									<textarea class="form-control" rows="3" name="ket"><?php echo $ket; ?></textarea>
									<?php
									if(isset($error['ket']))
							       	{
							           foreach($error['ket'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<button type="submit" class="btn btn-primary">Submit</button>	<!-- button untuk menyimpan data--> 
								<button type="reset" class="btn btn-default">Reset</button>	
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
		