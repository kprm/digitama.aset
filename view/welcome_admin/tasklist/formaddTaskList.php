		<div class="row">
			<div class="col-lg-12"> 
				<h1 class="page-header">Add Task List</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default"> 
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/tasklist/formAddTaskList" enctype="multipart/form-data">
							
								<!-- Form untuk mengisi jenis tugas-->
								<?php
								$haserror_jTugas = "";	//memberikan pesan error
								if(isset($error['jTugas']))
						       	{
						       		$haserror_jTugas = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_jTugas; ?>">
									<label>Jenis Tugas </label>
									<input class="form-control" placeholder="Masukkan jenis tugas" name="jTugas" value="<?php echo $jTugas; ?>">
									<?php
									if(isset($error['jTugas']))
							       	{
							           foreach($error['jTugas'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi proritas tugas-->
								<br><div class="form-group">
									<label>Proritas</label>
									<div class="radio">
										<label> <!-- yang terpilih awal adalah perempuan (checked)-->
											<input type="radio" name="prioritas" id="optionsRadios1" value="low">Low	
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="prioritas" id="optionsRadios2" value="middle">Middle
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="prioritas" id="optionsRadios3" value="high">High
									</div>
								</div>

								<!-- Form untuk mengisi tim yang mengerjakan-->
								<br><div class="form-group">
									<label>Tim yang mengerjakan</label>
									<div class="radio">
										<label> <!-- yang terpilih awal adalah perempuan (checked)-->
											<input type="radio" name="tim" id="optionsRadios1" value="art">Art	
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="tim" id="optionsRadios2" value="production">Production
									</div>
								</div>

								<!-- Form untuk mengisi nama orang yang mengerjakan tugas-->
								<?php
								$haserror_nama = "";	//memberikan pesan error
								if(isset($error['nama']))
						       	{
						       		$haserror_nama = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_nama; ?>">
									<label>Nama yang mengerjakan  </label>
									<input class="form-control" placeholder="Masukkan nama" name="nama" value="<?php echo $nama; ?>">
									<?php
									if(isset($error['nama']))
							       	{
							           foreach($error['nama'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi nama orang yang menjadi cadangan mengerjakan tugas-->
								<?php
								$haserror_nCadangan = "";	//memberikan pesan error
								if(isset($error['haserror_nCadangan']))
						       	{
						       		$haserror_nCadangan = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_nCadangan; ?>">
									<label>Nama cadangan yang mengerjakan  </label>
									<input class="form-control" placeholder="Masukkan nama cadangan" name="nCadangan" value="<?php echo $nCadangan; ?>">
									<?php
									if(isset($error['nCadangan']))
							       	{
							           foreach($error['nCadangan'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>
								
								<!-- Form untuk mengisi tanggal order-->	
								<?php
								$haserror_tgl = "";	//memberikan pesan error
								if((isset($error['tanggal'])) || (isset($error['bulan'])) || (isset($error['tahun'])))
						       	{
						       		$haserror_tgl = "has-error";
						       	}
								?>				
								<div class="form-group" <?php echo $haserror_tgl ?>>
									<label>Tanggal Order </label>
									<table>
										<tr>
											<td><select class="form-control" name="tanggal" placeholder="-Tanggal-">
											<option value="">-Tanggal-</option>

											<?php
											for($tgl=1;$tgl<=31;$tgl++){

													echo '<option value="'.$tgl.'">'.$tgl.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulan" placeholder="Bulan">
													<option value="">-Bulan-</option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahun">
											<option value="">-Tahun-</option>
											<?php

											$now = date('Y');
											$last = $now-150;
											for($thn=$now;$thn>=$last;$thn--){

													echo '<option value="'.$thn.'">'.$thn.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
									<?php
									if(isset($error['tanggal']))
							       	{
							           foreach($error['tanggal'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['bulan']))
							       	{
							           foreach($error['bulan'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['tahun']))
							       	{
							           foreach($error['tahun'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi tanggal mulai pengerjaan-->	
								<?php
								$haserror_tglMulai = "";	//memberikan pesan error
								if((isset($error['haserror_tanggalMulai'])) || (isset($error['bulanMulai'])) || (isset($error['tahunMulai'])))
						       	{
						       		$haserror_tglMulai = "has-error";
						       	}
								?>				
								<div class="form-group" <?php echo $haserror_tglMulai ?>>
									<label>Tanggal Mulai </label>
									<table>
										<tr>
											<td><select class="form-control" name="tanggalMulai" placeholder="-Tanggal-">
											<option value="">-Tanggal-</option>

											<?php
											for($tglMulai=1;$tglMulai<=31;$tglMulai++){

													echo '<option value="'.$tglMulai.'">'.$tglMulai.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulanMulai" placeholder="Bulan">
													<option value="">-Bulan-</option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahunMulai">
											<option value="">-Tahun-</option>
											<?php

											$now = date('Y');
											$last = $now-150;
											for($tahunMulai=$now;$tahunMulai>=$last;$tahunMulai--){

													echo '<option value="'.$tahunMulai.'">'.$tahunMulai.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
									<?php
									if(isset($error['tanggalMulai']))
							       	{
							           foreach($error['tanggalMulai'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['bulanMulai']))
							       	{
							           foreach($error['bulanMulai'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['tahunMulai']))
							       	{
							           foreach($error['tahunMulai'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>
								
								<!-- Form untuk mengisi tanggal selesai pengerjaan-->	
								<?php
								$haserror_tglSelesai = "";	//memberikan pesan error
								if((isset($error['haserror_tanggalSelesai'])) || (isset($error['bulanSelesai'])) || (isset($error['tahunSelesai'])))
						       	{
						       		$haserror_tglSelesai = "has-error";
						       	}
								?>				
								<div class="form-group" <?php echo $haserror_tglSelesai ?>>
									<label>Tanggal Selesai </label>
									<table>
										<tr>
											<td><select class="form-control" name="tanggalSelesai" placeholder="-Tanggal-">
											<option value="">-Tanggal-</option>

											<?php
											for($tglSelesai=1;$tglSelesai<=31;$tglSelesai++){

													echo '<option value="'.$tglSelesai.'">'.$tglSelesai.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulanSelesai" placeholder="Bulan">
													<option value="">-Bulan-</option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahunSelesai">
											<option value="">-Tahun-</option>
											<?php

											$now = date('Y');
											$last = $now-150;
											for($tahunSelesai=$now;$tahunSelesai>=$last;$tahunSelesai--){

													echo '<option value="'.$tahunSelesai.'">'.$tahunSelesai.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
									<?php
									if(isset($error['tanggalSelesai']))
							       	{
							           foreach($error['tanggalSelesai'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['bulanSelesai']))
							       	{
							           foreach($error['bulanSelesai'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['tahunSelesai']))
							       	{
							           foreach($error['tahunSelesai'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi hasil-->	
								 <?php
								$haserror_hasil = "";	//memberikan pesan error
								if(isset($error['hasil_asli']))
						       	{
						       		$haserror_hasil = "has-error";
						       	}
								?>					
								<div class="form-group <?php echo $haserror_hasil; ?>">
									<label>Hasil</label>
									<input type="file" name="hasil_asli" value="<?php echo $hasil_asli; ?>">
										<p class="help-block">Example block-level help text here.</p>
									<?php
										if(isset($error['hasil_asli']))
								       	{
								           foreach($error['hasil_asli'] as $err)
								            {
								                echo '<span class="help-block">'.$err.'</span>';
								            }
								       	}
									?> 
								</div> 

								<!-- Form untuk mengisi Keterangan-->
								<?php
								$haserror_keterangan = "";	//memberikan pesan error
								if(isset($error['Ket']))
						       	{
						       		$haserror_keterangan = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_keterangan; ?>">
									<label>Keterangan </label>
									<input class="form-control" placeholder="Masukkan Keterangan" name="ket" value="<?php echo $ket; ?>">
									<?php
									if(isset($error['ket']))
							       	{
							           foreach($error['ket'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>
								
								<button type="submit" class="btn btn-primary">Submit</button>	<!-- button untuk menyimpan data--> 
								<button type="reset" class="btn btn-default">Reset</button>	
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->