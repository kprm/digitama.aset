	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Tugas</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table data-toggle="table" data-url="/tasklist/ambildata"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc"> 
							<a href="/tasklist/formAddTaskList"><button type="submit" class="btn btn-primary">Add Task</button></a>
						    <thead>
						    <tr>
						        <!--<th data-field="id" data-sortable="true" >Id User</th>-->
						        <th data-field="nama" data-sortable="true">Nama</th>
						        <th data-field="tglorder"  data-sortable="true">Tanggal Order</th>
						        <th data-field="tglMulai"  data-sortable="true">Tanggal Mulai</th>
						        <th data-field="tglSelesai"  data-sortable="true">Tanggal Selesai</th>
						        <th data-field="Hasil_rename" data-formatter="getImage">Hasil</th>
						        <!-- <th data-field="foto_asli" data-sortable="true">Foto</th> -->
						        <th data-field="ket" data-sortable="true">Keterangan</th>
						        <th data-field="id" data-formatter="getPreview">Action</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		 
	<script type="text/javascript">
		 function getPreview(value, row) {
    		hh = '<div class="label label-primary"><a href="/tasklist/formEditTaskList?id='+value+'" font size="14" >Edit</a></div> ';	//tombol untuk aksi mengedit 
	    	hh += '<div class="label label-danger"><a href="#" class="hapus" dataid="'+value+'">Hapus</a></div>';	//tombol untuk aksi menghapus 
	      	return hh;
    	}

    	function getImage(value,row){
    		str='<img src="/public/fotoUser/' +value+'"  width="100" />' ;
    		return str;
    	}
	</script>


	
	</div><!--/.main-->