<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/public/css/bootstrap.min.css" rel="stylesheet">
	<link href="/public/css/bootstrap-table.css" rel="stylesheet">
	<link href="/public/css/styles.css" rel="stylesheet">
	<script src="/public/js/jqu"></script>
		<!--Icons-->
		<script src="/public/js/lumino.glyphs.js"></script>
</head>
<body>
	<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Profile</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/profil/updateProfil?id=<?php echo $id; ?>">
								<div class="form-group" align="right">
									<img src="/public/fotoUser/<?php echo $foto_rename; ?>" width="200" align="right">		
									<a href="#" onclick="myFunction('divfoto')" data-toggle="tooltip" title="Sunting foto" align="right"><span class="glyphicon glyphicon-pencil"></span></a>
									<div id="divfoto" style="display:none">
										<!-- <form method="post" action="/profil/settingProfil?id=<?php echo $id; ?>"> -->
											<input type="file" name="foto_asli" value="<?php echo $foto_asli; ?>"> 
											<?php echo $foto_asli; ?>
										<!-- </form> -->
									</div>
								</div>

								<div class="form-group">
									<label>Nama Lengkap </label>
									<?php echo $nama; ?>
									<a href="#" onclick="myFunction('divnama')" data-toggle="tooltip" title="Sunting nama"><span class="glyphicon glyphicon-pencil"></span></a>
									<div id="divnama" style="display:none">
										<!-- <form method="post" action="/profil/settingProfil?id=<?php echo $id; ?>"> -->
											<input name="nama" value="<?php echo $nama; ?>">
											<button type="submit" class="btn btn-primary"> Save </button>
										<!-- </form> -->
									</div>
								</div>
																
								<div class="form-group">
									<label>Tanggal Lahir </label>
									<?php echo $tanggal.$bulan.$tahun; ?>
									<a href="#" onclick="myFunction('divttl')" data-toggle="tooltip" title="Sunting tanggal lahir"><span class="glyphicon glyphicon-pencil"></span></a>
									<div id="divttl" style="display:none">
										<!-- <form method="post" action="/profil/settingProfil?id=<?php echo $id; ?>"> -->
											<table>
												<tr>
													<td><select class="form-control" name="tanggal">
													<option value="<?php echo $tanggal;?>"><?php echo $tanggal;?></option>

													<?php
													for($tgl=1;$tgl<=31;$tgl++){

															echo '<option value="'.$tgl.'">'.$tgl.'</option>';
													}
													?>
													</select></td>
													<td>
														<select class="form-control" name="bulan">
															<option value="<?php echo $bulan;?>"><?php echo $bulan;?></option>
															<option value="Januari">Januari</option>
															<option value="Februari">Februari</option>
															<option value="Maret">Maret</option>
															<option value="April">April</option>
															<option value="Mei">Mei</option>
															<option value="Juni">Juni</option>
															<option value="Juli">Juli</option>
															<option value="Agustus">Agustus</option>
															<option value="September">September</option>
															<option value="Oktober">Oktober</option>
															<option value="November">November</option>
															<option value="Desember">Desember</option>
														</select>
													</td>
													<td><select class="form-control" name="tahun">
													<option value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
													<?php

													$now = date('Y');
													$last = $now-150;
													for($thn=$now;$thn>=$last;$thn--){

															echo '<option value="'.$thn.'">'.$thn.'</option>';
													}
													?>
													</select></td>
												</tr>
											</table>
											<button type="submit" class="btn btn-primary"> Save </button>
										<!-- </form> -->
									</div>
								</div>

								<div class="form-group">
									<label>Jenis Kelamin </label>
									<?php echo $jk; ?>
									<a href="#" onclick="myFunction('divjk')" data-toggle="tooltip" title="Sunting jenis kelamin"><span class="glyphicon glyphicon-pencil"></span></a>
									<div id="divjk" style="display:none">
										<!-- <form method="post" action="/user/updateUser?id=<?php echo $id; ?>"> -->
											<div class="radio">
												<label> <!-- yang terpilih awal adalah perempuan (checked)-->
													<input type="radio" name="jk" id="optionsRadios1" value="Perempuan" <?php echo ($jk=='Perempuan')?'checked':'' ?>  >Perempuan	
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="jk" id="optionsRadios2" value="Laki-Laki" <?php echo ($jk=='Laki-Laki')?'checked':'' ?>>Laki - laki
											</div>
											<button type="submit" class="btn btn-primary"> Save </button>
										<!-- </form> -->
									</div>
								</div>

								<div class="form-group">
									<label>Alamat</label>
									<?php echo $alamat; ?>
									<a href="#" onclick="myFunction('dival')" data-toggle="tooltip" title="Sunting alamat"><span class="glyphicon glyphicon-pencil"></span></a>
									<div id="dival" style="display:none">
										<form method="post" action="/user/updateUser?id=<?php echo $id; ?>">
											<input name="alamat" value="<?php echo $alamat; ?>">
											<button type="submit" class="btn btn-primary"> Save </button>
										</form>
									</div>
								</div>

								<div class="form-group">
									<label>Email </label>
									<?php echo $email; ?>
									<a href="#" onclick="myFunction('divemail')" data-toggle="tooltip" title="Sunting email"><span class="glyphicon glyphicon-pencil"></span></a>
									<div id="divemail" style="display:none">
										<!-- <form method="post" action="/user/updateUser?id=<?php echo $id; ?>"> -->
											<input name="email" value="<?php echo $email; ?>">
											<button type="submit" class="btn btn-primary"> Save </button>
										<!-- </form> -->
									</div>
								</div>

								<div class="form-group">
									<label> Password </label>
									<?php echo $pass; ?>
									<a href="#" onclick="myFunction('divpass')" data-toggle="tooltip" title="Sunting password"><span class="glyphicon glyphicon-pencil"></span></a>
									<div id="divpass" style="display:none">
										<!-- <form method="post" action="/user/updateUser?id=<?php echo $id; ?>"> -->
											<input name="pass" value="<?php echo $pass; ?>">
											<button type="submit" class="btn btn-primary"> Save </button>
										<!-- </form> -->
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		<script type="text/javascript">
		 
		
    	function getImage(value,row){
    		str='<img src="/public/fotoUser/' +value+'"  width="100" />' ;
    		return str;
    	}

    	$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});

		//source code menampilkan dan menyembunyikan form edit profil
		function myFunction(id){
             if(document.getElementById(id).style.display==""){
                 document.getElementById(id).style.display="none";
             }else{
                 document.getElementById(id).style.display="";
             }
       } 
	       myFunction('divfoto');
	       myFunction('divnama');
	       myFunction('divemail');
	       myFunction('divttl');
	       myFunction('dival');
	       myFunction('divjk');
	       myFunction('divpass');
       
	</script>
</body>
</html>