		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Detail</h1>
			</div>
		</div><!--/.row-->


		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php 

						if($tipe=="Picture"){
							echo '<center><div class="gambar"><center><img src="/public/assets/pict/'.$file_rename.'"></div></center>';
						}
						if($tipe=="Sketch"){
							echo '<center><div class="gambar"><img src="/public/assets/sketch/'.$file_rename.'"></div></center>';
						}

						?>


							<br>		
						<center><a href="/aset/download?id=<?php echo $id; ?>" class="label label-success"><b>Download</b></a>
							

						<?php 
							if($idUser==$_SESSION['id']){
								echo '<a href="/aset/formEditAset?id='.$id.'" class="label label-primary"><b>Update</b></a>';
							}
						?>
						</center>

					</div>
				</div>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="#">
							

								<div class="form-group">
									<label>Pemilik</label>
									<?php echo $pemilik; ?>
								</div>
																
								<div class="form-group">
									<label>Judul </label>
									<?php echo $judul; ?>
								</div>

								<div class="form-group">
									<label>Tipe </label>
										<?php echo $tipe; ?>
								</div>

								<div class="form-group">
									<label>Group</label>
									<?php echo $namaGrup; ?>

								</div>

								<div class="form-group">
									<label>Time Upload </label>
									<?php echo $timeUpload; ?>
								</div>

								<div class="form-group">
									<label> Last Update</label>
									<?php echo $lastUpdate; ?>
								</div>
								<div class="form-group">
									<label> Deskripsi</label>
									<?php echo $desk; ?>
								</div>
								<div class="form-group">
									<label> Status</label>
									<?php echo $status; ?>
								</div>
								<!-- <div class="form-group">
									
									<a href="/aset/download?id=<?php echo $id; ?>" class="label label-success"><b>Download</b></a>
								</div>

								<?php 
									//if($idUser==$_SESSION['id']){
										//echo '<a href="/aset/formEditAset?id='.$id.'" class="label label-primary"><b>Update</b></a>';
									//}
								?> -->
																
								
								
								<!--<label>Validation</label>
								<div class="form-group has-success">
									<input class="form-control" placeholder="Success">
								</div>
								<div class="form-group has-warning">
									<input class="form-control" placeholder="Warning">
								</div>
								<div class="form-group has-error">
									<input class="form-control" placeholder="Error">
								</div>
								
							</div>
							<div class="col-md-6">
							
								<div class="form-group">
									<label>Checkboxes</label>
									<div class="checkbox">
										<label>
											<input type="checkbox" value="">Checkbox 1
										</label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" value="">Checkbox 2
										</label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" value="">Checkbox 3
										</label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" value="">Checkbox 4
										</label>
									</div>
								</div>-->
											
								<!--<div class="form-group">
									<label>Multiple Selects</label>
									<select multiple class="form-control">
										<option>Option 1</option>
										<option>Option 2</option>
										<option>Option 3</option>
										<option>Option 4</option>
									</select>
								</div>-->
								
								<!-- <button type="submit" class="btn btn-primary">Ubah</button> -->
								
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

		<div class="panel panel-default chat">
					<div class="panel-body">
						<ul>

						<?php 
				
						foreach ($dataa as $dtaa) {

							$db = Db::init();	
							$col = $db -> users;
							$data = $col -> findone (	//untuk memilih satu
								array(
									"_id" => new mongoid($dtaa['idCommenter'])
									)
								);

							$commenter=$data['nama'];
							$foto=$data['foto_rename'];
												

							echo '


							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="/public/fotoUser/'.$foto.'" alt="User Avatar" class="img-circle" width="70" height="70" />
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font"> '.$commenter.'</strong> <small class="text-muted">'.$dtaa['waktu'].'</small>
									</div>
									<p>
										'.$dtaa['comment'].'
									</p>
								</div>
							</li>

							';
						}
						?>
							
						</ul>
					</div>
					<div class="panel-footer">
						<div class="input-group">
							<form role="form" method="post" action="/comment/addComment">
							<input type="hidden" name="idAset" value="<?php echo $id; ?>"></input>
							<input type="hidden" name="pemilik" value="<?php echo $pemilik; ?>"></input>
							<input type="hidden" name="idpemilik" value="<?php echo $idUser; ?>"></input>
							<input id="btn-input" name="comment" type="text" class="form-control input-md" placeholder="Type your message here..." />
							
							<!-- <button class="btn btn-success btn-md" id="btn-chat">Send</button> -->
							<span class="input-group-btn">	<input type="submit" name="submit" class="btn btn-success btn-md" id="btn-chat" value="send"></input> </span>
							
							
							</form>
						</div>
					</div>
				</div>
				
			</div><!--/.col-->