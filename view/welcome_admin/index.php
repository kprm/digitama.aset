		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Beranda</h1> 
			</div>
		</div><!--/.row-->


		<div class="row">
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-blue panel-widget ">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large"><?php echo $aset; ?></div>
							<div class="text-muted">Assets</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-teal panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large"><?php echo $user; ?></div>
							<div class="text-muted">Users</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-orange panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked empty-message"><use xlink:href="#stroked-empty-message"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large"><?php echo $grup; ?></div>
							<div class="text-muted">Group</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-6 col-lg-3">
				<div class="panel panel-red panel-widget">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-5 widget-left">
							<svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"></use></svg>
						</div>
						<div class="col-sm-9 col-lg-7 widget-right">
							<div class="large"><?php echo $project; ?></div>
							<div class="text-muted">Project</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h2><strong>Gambar Terbaru</strong></h2>
				<hr>
				<a href="/welcomeadmin/viewAllPict"><h5 align="right"> View All </h5></a>
			</div>
		</div>
		
		<div class="my-gallery-container">
		 	<?php 
				foreach ($data as $dt) {

					$db = Db::init();	
					$col = $db -> users;
					$data = $col -> findone (	//untuk memilih satu
						array(
							"_id" => new mongoid($dt['idUser'])
							)
						);

					$nama=$data['nama'];
					$foto=$data['foto_rename'];
					echo '
				 	    <div class="item"> 
			            	<div class="thumbnail">
			                	<div class="caption">
				                    <h4>'.$dt['judul'].'</h4>
				                    <p>'.$dt['desk'].'</p>
				                    <p><a href="/aset/detail?id='.$dt['_id'].'" class="label label-danger">View</a></p>
			                	</div>
			                    
			                	<img class="img-responsive" src="/public/assets/pict/'.$dt['file_rename'].'" width="250">
			            	</div>
			            </div>
			        ';
			    }			 
			?>
		</div>
		


		<div class="row">
			<div class="col-lg-12">
				<h2>Sketsa Terbaru</h2>
				<hr>
				<a href="/welcomeadmin/viewAllSketch"><h5 align="right"> View All </h5></a>
			</div>
		</div>


		<div class="my-gallery-container">
		 	<?php 

				
				foreach ($data2 as $dt2) {

					$db = Db::init();	
					$col = $db -> users;
					$data = $col -> findone (	//untuk memilih satu
						array(
							"_id" => new mongoid($dt2['idUser'])
							)
						);

					$nama=$data['nama'];
					$foto=$data['foto_rename'];
					echo '
				 	        <div class="item"> 
			            <div class="thumbnail">
			                <div class="caption">
			                    <h4>'.$dt2['judul'].'</h4>
			                    <p>'.$dt2['desk'].'</p>
			                    <p><a href="/aset/detail?id='.$dt2['_id'].'" class="label label-danger">View</a></p>
			                </div>
			                
			                <img class="img-responsive" src="/public/assets/sketch/'.$dt2['file_rename'].'" width="250">
			             </div>
			            </div>
			        ';
			    }			 
			?>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<h2>MP3 Terbaru</h2>
				<hr>
				<a href="/welcomeadmin/viewAllMp3"><h5 align="right"> View All </h5></a>
			</div>
		</div>

		
		<div class="my-gallery-container">
			<?php 
			
				
				foreach ($data3 as $dt3) {

					$db = Db::init();	
							$col = $db -> users;
							$data = $col -> findone (	//untuk memilih satu
								array(
									"_id" => new mongoid($dt3['idUser'])
									)
								);

							$nama=$data['nama'];
							$foto=$data['foto_rename'];

					echo '
						<div class="item"> 
			            	<div class="thumbnail">
	
			                    
			                	<center><a href="/aset/detail?id='.$dt3['_id'].'" class="label label-danger">'.$dt3['judul'].'</a></center><br>
						<center><div class="ui360">
				 			<a href="/public/assets/audio/'.$dt3['file_rename'].'"></a>
						</div></center>
			            	</div>
			            </div>


						

			';
				}
			?>
			
		</div>
		

		


		<div class="row">
			<div class="col-lg-12">
				<h2>3D Modelling Terbaruuuu</h2>
				<hr>
				<a href="/welcomeadmin/viewAll3d"><h5 align="right"> View All </h5></a>
			</div>
		</div>

		<div class="my-gallery-container">
		 	<?php 
				foreach ($data4 as $dt4) {

					$db = Db::init();	
					$col = $db -> users;
					$data = $col -> findone (	//untuk memilih satu
						array(
							"_id" => new mongoid($dt['idUser'])
							)
						);

					$nama=$data['nama'];
					$foto=$data['foto_rename'];
					echo '
				 	    <div class="item"> 
			            	<div class="thumbnail">
			                	<div class="caption">
				                    <h4>'.$dt4['judul'].'</h4>
				                    <p>'.$dt4['desk'].'</p>
				                    <p><a href="/aset/detail?id='.$dt4['_id'].'" class="label label-danger">View</a></p>
			                	</div>
			                    
			                	<img class="img-responsive" src="/public/assets/3d/3d.png" width="250">
			            	</div>
			            </div>
			        ';
			    }			 
			?>
		</div>


		<!-- 



		<div class="row">
			<div class="col-lg-12">
				<h2>3D Modelling Terbaru</h2>
				<hr>
				<a href="/welcomeadmin/viewAll3d"><h5 align="right"> View All </h5></a>
			</div>
		</div>

		<div class="container">
		<a href="/welcomeadmin/view3d"><img src="/public/assets/3d/3d.png" width="150"></a>
			<?php 
				
				// foreach ($data4 as $dt4) {

				// 	echo '<div class="col-md-4">
				// <div class="panel panel-default">
				// 	<div class="panel-heading">
				// 			<center><img src="/public/assets/3d/'.$dt4['file_rename'].'"  height="200" /></center>
				// 	</div>
				// 	<div class="panel-body">
				// 		<p><a href="/aset/detail?id='.$dt2['_id'].'">'.$dt4['desk'].'</a></p>
				// 	</div>
				// </div>
			// </div>';
			// 	}
			// ?>
			// </div> -->

		
		
		