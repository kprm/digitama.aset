	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar Group</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table data-toggle="table" data-url="/group/ambildataGroup"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc"> 
							<a href="/group/formAddGroup"><button type="submit" class="btn btn-primary">Add Group</button></a>
						    <thead>
						    <tr>
						        <th data-field="_id" data-checkbox="true" >Id Gambar</th>
						        <th data-field="namaGrup" data-sortable="true">Nama Grup</th>
						        <th data-field="namaProject" data-sortable="true">Project</th>						    
						        <th data-field="tglMulai"  data-sortable="true">Tanggal Mulai</th>
						        <th data-field="status" data-sortable="true">Status</th>
						        <th data-field="ket" data-sortable="true">Keterangan</th>
						        <th data-field="id" data-formatter="getPreview">Action</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		
	<script type="text/javascript">
		 function getPreview(value, row) {
    		hh = '<div class="label label-primary"><a href="/group/formEditGroup?id='+value+'" >Edit</a></div>';	//tombol untuk aksi mengedit 
	    	hh += '<div class="label label-danger"><a href="#" class="hapusGroup" dataid="'+value+'">Hapus</a></div>';	//tombol untuk aksi menghapus 
	      	return hh;

    }
	</script>
	
	</div><!--/.main-->ekao