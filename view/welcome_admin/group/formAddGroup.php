		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Add Group</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/group/formAddGroup">
							
								<!-- Form untuk mengisi nama-->
								<?php
								$haserror_namaGroup = "";	//memberikan pesan error
								if(isset($error['namaGroup']))
						       	{
						       		$haserror_namaGroup = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_namaGroup; ?>">
									<label> Group </label>
									<input class="form-control" placeholder="Masukkan nama Group" name="namaGrup" value="<?php echo $namaGrup; ?>">
									<?php
									if(isset($error['namaGroup']))
							       	{
							           foreach($error['namaGroup'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi project-->
								<?php
								$haserror_project = "";	//memberikan pesan error
								if(isset($error['project']))
						       	{
						       		$haserror_project = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_project; ?>">
									<label> Project </label>
									<select name="idProject" class="form-control">
    									<option value="">-Pilih Project-</option>
    									<?php
								       	$db = Db::init();
										$col = $db -> project;
										$dt=$col->find();	// memilih semua == select all
										$data = array();
										foreach($dt as $dta)
										{
					

										echo "<option value='".$dta['_id']."'> ".$dta['namaProject']." </option>";

										}
										?>
									</select>
									<?php
									if(isset($error['project']))
							       	{
							           foreach($error['project'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>
								
								
								<!-- Form untuk mengisi tgl mulai-->
								<?php
								$haserror_tglMulai = "";	//memberikan pesan error
								if(isset($error['tglMulai']))
						       	{
						       		$haserror_tglMulai = "has-error";
						       	}
								?>			
								<div class="form-group">
									<label>Tanggal Mulai</label>
									<table>
										<tr>
											<td><select class="form-control" name="tgl">
											<option value="">-Tanggal-</option>

											<?php
											for($tgl=1;$tgl<=31;$tgl++){

													echo '<option value="'.$tgl.'">'.$tgl.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulan">
													<option value="">-Bulan-</option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahun">
											<option value="">-Tahun</option>
											<?php

											$now = date('Y');
											$last = $now+5;
											for($thn=$now;$thn<=$last;$thn++){

													echo '<option value="'.$thn.'">'.$thn.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
								</div>
							

								<!-- Form untuk mengisi status-->
								<div class="form-group <?php echo $haserror_status; ?>">
									<label>Status </label>
										<select class="form-control" name="status" value="<?php echo $status; ?>">
											<option value="Belum">Belum</option>
											<option value="Proses">Proses</option>
											<option value="Selesai">Selesai</option>
										</select>		
											
								</div>

								<!-- Form untuk mengisi keterangan-->
								<?php
								$haserror_ket = "";	//memberikan pesan error
								if(isset($error['ket']))
						       	{
						       		$haserror_ket = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_ket ?>">
									<label>Keterangan</label>
									<textarea class="form-control" rows="3" name="ket" placeholder="Masukkan Keterangan"><?php echo $ket; ?></textarea>
									<?php
									if(isset($error['ket']))
							       	{
							           foreach($error['ket'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								
								
								<button type="submit" class="btn btn-primary">Submit</button>	<!-- button untuk menyimpan data--> 
								<button type="reset" class="btn btn-default">Reset</button>	
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
		