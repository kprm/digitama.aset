	
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Daftar User</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<table data-toggle="table" data-url="/user/ambildata"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc"> 
							<a href="/user/formAddUser"><button type="submit" class="btn btn-primary">Add User</button></a>
						    <thead>
						    <tr>
						        <!--<th data-field="id" data-sortable="true" >Id User</th>-->
						        <th data-field="nama" data-sortable="true">Nama</th>
						        <th data-field="tanggallahir"  data-sortable="true">Tanggal Lahir</th><!-- 
						        <th data-field="bulan" data-sortable="true">Bulan</th>
						        <th data-field="tahun" data-sortable="true">Tahun</th> -->
						        <th data-field="jk"  data-sortable="true">Jenis Kelamin</th>
						        <th data-field="alamat" data-sortable="true">Alamat</th>
						        <th data-field="email" data-sortable="true">Email</th>
						        <th data-field="telepon" data-sortable="true">Telepon</th>
						        <!--<th data-field="pass" data-sortable="true">Password</th>-->
						        <th data-field="foto_rename" data-formatter="getImage">Foto</th>
						        <!-- <th data-field="foto_asli" data-sortable="true">Foto</th> -->
						        <th data-field="status" data-sortable="true">Status</th>
						        <th data-field="bagian" data-sortable="true">Bagian</th>
						        <!-- <th data-field="bagian" data-sortable="true">Bagian</th> -->
						        <th data-field="id" data-formatter="getPreview">Action</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
		 
	<script type="text/javascript">
		 function getPreview(value, row) {
    		hh = '<div class="label label-primary"><a href="/user/formEditUser?id='+value+'" >Edit</a></div> ';	//tombol untuk aksi mengedit 
	    	hh += '<div class="label label-danger"><a href="#" class="hapus" dataid="'+value+'">Hapus</a></div>';	//tombol untuk aksi menghapus 
	      	return hh;
    	}

    	function getImage(value,row){
    		str='<img src="/public/fotoUser/' +value+'"  width="100" />' ;
    		return str;
    	}
	</script>


	
	</div><!--/.main-->