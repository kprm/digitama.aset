		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Edit User</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/user/updateUser?id=<?php echo $id; ?>" enctype="multipart/form-data">

								<!-- Form untuk mengedit nama-->
								<?php
								$haserror_nama = "";	//memberikan pesan error
								if(isset($error['nama']))
						       	{
						       		$haserror_nama = "has-error";
						       	}
								?>

								<div class="form-group">
									<label>Nama Lengkap </label>
									<input class="form-control" name="nama" value="<?php echo $nama; ?>">
									<?php
									if(isset($error['nama']))
							       	{
							           foreach($error['nama'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>
										
								<!-- Form untuk mengisi tanggal lahir-->	
								<?php
								$haserror_tgl = "";	//memberikan pesan error
								if((isset($error['tanggal'])) || (isset($error['bulan'])) || (isset($error['tahun'])))
						       	{
						       		$haserror_tgl = "has-error";
						       	}
								?>

								<div class="form-group">
									<label>Tanggal Lahir </label>
									<table>
										<tr>
											<td><select class="form-control" name="tanggal">
											<option value="<?php echo $tanggal;?>"><?php echo $tanggal;?></option>

											<?php
											for($tgl=1;$tgl<=31;$tgl++){

													echo '<option value="'.$tgl.'">'.$tgl.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulan">
													<option value="<?php echo $bulan;?>"><?php echo $bulan;?></option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahun">
											<option value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
											<?php

											$now = date('Y');
											$last = $now-150;
											for($thn=$now;$thn>=$last;$thn--){

													echo '<option value="'.$thn.'">'.$thn.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>

									<?php
									if(isset($error['tanggal']))
							       	{
							           foreach($error['tanggal'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['bulan']))
							       	{
							           foreach($error['bulan'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['tahun']))
							       	{
							           foreach($error['tahun'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi jenis kelamin-->
								<br><div class="form-group"
									<label>Jenis Kelamin</label>
									<div class="radio">
										<label> <!-- yang terpilih awal adalah perempuan (checked)-->
											<input type="radio" name="jk" id="optionsRadios1" value="Perempuan" <?php echo ($jk=='Perempuan')?'checked':'' ?>  >Perempuan	
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="jk" id="optionsRadios2" value="Laki-Laki" <?php echo ($jk=='Laki-Laki')?'checked':'' ?>>Laki - laki
									</div>
								</div>


								<!-- Form untuk mengedit alamat-->
								<?php
								$haserror_alamat = "";	//memberikan pesan error
								if(isset($error['alamat']))
						       	{
						       		$haserror_alamat = "has-error";
						       	}
								?>
								<div class="form-group">
									<label>Alamat</label>
									<textarea class="form-control" rows="3" name="alamat"><?php echo $alamat; ?></textarea>
									<?php
									if(isset($error['alamat']))
							       	{
							           foreach($error['alamat'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengedit email-->
								<div class="form-group">
								<?php
								$haserror_email = "";	//memberikan pesan error
								if(isset($error['email']))
						       	{
						       		$haserror_email = "has-error";
						       	}
								?>
									<label>Email </label>
									<input class="form-control"  name="email" value="<?php echo $email; ?>">
									<?php
									if(isset($error['email']))
							       	{
							           foreach($error['email'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengedit tlp-->
								<div class="form-group">
									<label>Telepon</label>
									<input class="form-control"  name="telepon" value="<?php echo $telepon; ?>">
									<?php
									if(isset($error['telepon, prefix)']))
							       	{
							           foreach($error['telepon'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengedit password-->
								<!-- <div class="form-group">
									<label> Password </label>
									<input type="password" class="form-control" name="pass" value="<?php echo $pass; ?>">
								</div> -->
									
								<!-- Form untuk mengedit foto-->							
												
								<!-- <div class="form-group">
									<label>Foto</label>
									<input type="file" name="foto_asli" value="<?php echo $foto_asli; ?>"> 
									<?php echo $foto_asli; ?>
									 <!--<p class="help-block">Example block-level help text here.</p>-->
								<!-- </div> -->


								<!-- Form untuk mengisi status-->
								<br><div class="form-group">
									<label>Status</label>
									<div class="radio">
										<label>
											<input type="radio" name="status" id="Radios1" value="User" <?php echo ($status=='User')?'checked':'' ?> >User
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="status" id="Radios2" value="Admin" <?php echo ($status=='Admin')?'checked':'' ?> >Admin
									</div>
								</div>


								
								<!-- Form untuk mengisi bagian-->	
								<?php
								$haserror_bagian = "";	//memberikan pesan error
								if((isset($error['bagian'])))
						       	{
						       		$haserror_bagian = "has-error";
						       	}
								?>				
								<div class="form-group" <?php echo $haserror_bagian; ?>>
									<label>Bagian </label>
										<select class="form-control" name="bagian">
											<option value="<?php echo $bulan;?>"><?php echo $bulan;?></option>
											<option value="Progammer">Programmer</option>
											<option value="Multimedia(Art)">Multimedia(Art)</option>
											<option value="Multimedia(Produksi)">Multimedia(Produksi)</option>
										</select>
											
									<?php
									if(isset($error['bagian']))
							       	{
							           foreach($error['bagian'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	
									?>
								</div>


								<button type="submit" class="btn btn-primary"> Save </button>	<!-- button untuk menyimpan hasil edit-->
								<button type="reset" class="btn btn-default"> Reset </button>	<!-- button untuk mereset semua form isian-->
							</form>
						</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->