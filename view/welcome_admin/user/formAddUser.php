		<div class="row">
			<div class="col-lg-12"> 
				<h1 class="page-header">Add User</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default"> 
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/user/formAddUser" enctype="multipart/form-data">
							
								<!-- Form untuk mengisi nama-->
								<?php
								$haserror_nama = "";	//memberikan pesan error
								if(isset($error['nama']))
						       	{
						       		$haserror_nama = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_nama; ?>">
									<label>Nama Lengkap </label>
									<input class="form-control" placeholder="Masukkan Nama" name="nama" value="<?php echo $nama; ?>">
									<?php
									if(isset($error['nama']))
							       	{
							           foreach($error['nama'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            }
							       	}
									?>
								</div>
								
								<!-- Form untuk mengisi tanggal lahir-->	
								<?php
								$haserror_tgl = "";	//memberikan pesan error
								if((isset($error['tanggal'])) || (isset($error['bulan'])) || (isset($error['tahun'])))
						       	{
						       		$haserror_tgl = "has-error";
						       	}
								?>				
								<div class="form-group" <?php echo $haserror_tgl ?>>
									<label>Tanggal Lahir </label>
									<table>
										<tr>
											<td><select class="form-control" name="tanggal" placeholder="-Tanggal-">
											<option value="">-Tanggal-</option>

											<?php
											for($tgl=1;$tgl<=31;$tgl++){

													echo '<option value="'.$tgl.'">'.$tgl.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulan" placeholder="Bulan">
													<option value="">-Bulan-</option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahun">
											<option value="">-Tahun-</option>
											<?php

											$now = date('Y');
											$last = $now-150;
											for($thn=$now;$thn>=$last;$thn--){

													echo '<option value="'.$thn.'">'.$thn.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
									<?php
									if(isset($error['tanggal']))
							       	{
							           foreach($error['tanggal'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['bulan']))
							       	{
							           foreach($error['bulan'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	if(isset($error['tahun']))
							       	{
							           foreach($error['tahun'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>


								


								<!-- Form untuk mengisi jenis kelamin-->
								<br><div class="form-group">
									<label>Jenis Kelamin</label>
									<div class="radio">
										<label> <!-- yang terpilih awal adalah perempuan (checked)-->
											<input type="radio" name="jk" id="optionsRadios1" value="Perempuan">Perempuan	
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="jk" id="optionsRadios2" value="Laki-Laki">Laki - laki
									</div>
								</div>

								<!-- Form untuk mengisi alamat-->
								<?php
								$haserror_alamat = "";	//memberikan pesan error
								if(isset($error['alamat']))
						       	{
						       		$haserror_alamat = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_alamat ?>">
									<label>Alamat</label>
									<textarea class="form-control" rows="3" name="alamat" placeholder="Masukkan Alamat"><?php echo $alamat; ?></textarea>
									<?php
									if(isset($error['alamat']))
							       	{
							           foreach($error['alamat'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi email-->
								<?php
								$haserror_email = "";	//memberikan pesan error
								if(isset($error['email']))
						       	{
						       		$haserror_email = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_email; ?>">
									<label>Email </label>
									<input class="form-control" placeholder="Masukkan Email" name="email" value="<?php echo $email; ?>">
									<?php
									if(isset($error['email']))
							       	{
							           foreach($error['email'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>


								<!-- Form untuk mengisi tlp-->
								<?php
								$haserror_telepon = "";	//memberikan pesan error
								if(isset($error['telepon']))
						       	{
						       		$haserror_telepon = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_telepon; ?>">
									<label>Telepon</label>
									<input class="form-control" placeholder="Masukkan No Telepon" name="telepon" value="<?php echo $telepon; ?>">
									<?php
									if(isset($error['telepon, prefix)']))
							       	{
							           foreach($error['telepon'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi password-->
								<?php
								$haserror_password = "";	//memberikan pesan error
								if(isset($error['pass']))
						       	{
						       		$haserror_password = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_nama; ?>">
									<label> Password </label>
									<input type="password" class="form-control" placeholder="Masukkan Password" name="pass" value="<?php echo $pass; ?>">
									<?php
									if(isset($error['pass']))
							       	{
							           foreach($error['pass'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

						<!-- Form untuk mengisi foto-->	
								<!-- <?php
								$haserror_foto = "";	//memberikan pesan error
								if(isset($error['foto_asli']))
						       	{
						       		$haserror_foto = "has-error";
						       	}
								?>					
								<div class="form-group <?php echo $haserror_foto; ?>">
									<label>Foto</label>
									<input type="file" name="foto_asli" value="<?php echo $foto_asli; ?>">
									 <!--<p class="help-block">Example block-level help text here.</p>-->
								<!-- <?php
									if(isset($error['foto_asli']))
							       	{
							           foreach($error['foto_asli'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?> 
								</div> -->


								<!-- Form untuk mengisi status-->
								<br><div class="form-group">
									<label>Status</label>
									<div class="radio">
										<label>
											<input type="radio" name="status" id="Radios1" value="User" checked>User
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="status" id="Radios2" value="Admin">Admin
									</div>
								</div>

								<!-- Form untuk mengisi bagian-->	
								<?php
								$haserror_bagian = "";	//memberikan pesan error
								if((isset($error['bagian'])))
						       	{
						       		$haserror_bagian = "has-error";
						       	} 
								?>				
								<div class="form-group" <?php echo $haserror_bagian; ?>>
									<label>Bagian </label>
										<select class="form-control" name="bagian">
											<option value="">-Bagian-</option>
											<option value="Progammer">Programmer</option>
											<option value="Multimedia(Art)">Multimedia(Art)</option>
											<option value="Multimedia(Produksi)">Multimedia(Produksi)</option>
										</select>
											
									<?php
									if(isset($error['bagian']))
							       	{
							           foreach($error['bagian'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
							       	
									?>
								</div>


								
								
								<button type="submit" class="btn btn-primary">Submit</button>	<!-- button untuk menyimpan data--> 
								<button type="reset" class="btn btn-default">Reset</button>	
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->