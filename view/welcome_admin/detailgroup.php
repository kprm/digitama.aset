		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Detail Group</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="#">
							
								<div class="form-group">
									<label>Nama Group/Project</label>
									<?php echo $namaGroup; ?>
								</div>
																
								<div class="form-group">
									<label>Tanggal Mulai </label>
									<?php echo $tglMulai; ?>
								</div>

								<div class="form-group">
									<label>Status </label>
										<?php echo $status; ?>					
								</div>
							</form>
						</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

		<div class="row">
			
			<?php 
				
				foreach ($data as $dt) {

					$db = Db::init();	
							$col = $db -> users;
							$data = $col -> findone (	//untuk memilih satu
								array(
									"_id" => new mongoid($dt['idUser'])
									)
								);

							$nama=$data['nama'];
							$foto=$data['foto_rename'];

					if ($dt['tipe']=="Picture"){

					echo '<div class="col-md-4">
				<div class="panel panel-default"> 
					<div class="panel-heading">
							<center><a href="/aset/detail?id='.$dt['_id'].'"><img src="/public/assets/pict/'.$dt['file_rename'].'" height="200" /></a></center>
					</div>
					<div class="panel-body">
						<span class="chat-img pull-left">
									<img src="/public/fotoUser/'.$foto.'" alt="User Avatar" class="img-circle" width="60" height="60" />
								</span>
						
						<div class="header">
							<strong class="primary-font"> '.$nama.'</strong> <small class="text-muted">'.$dt['timeUpload'].'</small>
						</div>
						<p>'.$dt['desk'].'</p>
						
					</div>
				</div> 
				';
				} else if ($dt['tipe']=="Sketch"){

					echo '<div class="col-md-4">
				<div class="panel panel-default"> 
					<div class="panel-heading">
							<center><a href="/aset/detail?id='.$dt['_id'].'"><img src="/public/assets/sketch/'.$dt['file_rename'].'" height="200" /></a></center>
					</div>
					<div class="panel-body">
						<span class="chat-img pull-left">
									<img src="/public/fotoUser/'.$foto.'" alt="User Avatar" class="img-circle" width="60" height="60" />
								</span>
						
						<div class="header">
							<strong class="primary-font"> '.$nama.'</strong> <small class="text-muted">'.$dt['timeUpload'].'</small>
						</div>
						<p>'.$dt['desk'].'</p>
						
					</div>
				</div> 
				';
				} else if ($dt['tipe']=="Audio") {
					echo '
						<div class="col-md-6">
							<div class="panel panel-blue">
								<div class="panel-body"><a href="/aset/detail?id='.$dt['_id'].'"><strong class="primary-font" color="white">'.$dt['judul'].''.".mp3".'</strong></a>
								
									<audio src="/public/assets/'.$dt['file_rename'].'" preload="auto" /></div>
								<div class="panel-body">
									<span class="chat-img pull-left">
									<img src="/public/fotoUser/'.$foto.'" alt="User Avatar" class="img-circle" width="60" height="60" />
								</span>
								
								<div class="header">
									<strong class="primary-font"> '.$nama.'</strong> <small class="text-muted">'.$dt['timeUpload'].'</small>
								</div>
								<p>'.$dt['desk'].'</p>
							</div>
						</div>
						
					';				
				}
			}
			?>
		</div>