		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Profile</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/profil/settingProfilUser?id=<?php echo $id; ?>">
							

								<div class="form-group" align="right" data-field="foto" data-formatter="getImage">
									<img src="/public/fotoUser/<?php echo $foto_rename; ?>" width="200" align="right">
									<br>
								</div>

								<div class="form-group">
									<label>Nama Lengkap </label>
									<?php echo $nama; ?>
								</div>
																
								<div class="form-group">
									<label>Tanggal Lahir </label>
									
											<?php echo $tanggal.$bulan.$tahun; ?>
								</div>

								<div class="form-group">
									<label>Jenis Kelamin </label>
											<?php echo $jk; ?>
								</div>

								<div class="form-group">
									<label>Alamat</label>
									<?php echo $alamat; ?>

								</div>

								<div class="form-group">
									<label>Email </label>
									<?php echo $email; ?>
								</div>

								<div class="form-group">
									<label> Password </label>
									<?php echo $pass; ?>
								</div>
																
								<button type="submit" class="btn btn-primary">Ubah</button>
								
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		<script type="text/javascript">
		 

    	function getImage(value,row){
    		str='<img src="/public/fotoUser/' +value+'"  width="100" />' ;
    		return str;
    	}
	</script>
		
		