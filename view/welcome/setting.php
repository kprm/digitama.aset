		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Setting</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form">
							
								<div class="form-group">
									<label>Nama Lengkap </label>
									<input class="form-control" placeholder="Masukkan Nama">
								</div>
																
								<div class="form-group">
									<label>Tanggal Lahir </label>
									<table>
										<tr>
											<td><select class="form-control" name="tanggal">
											<option value="<?php echo $tanggal;?>"><?php echo $tanggal;?></option>

											<?php
											for($tgl=1;$tgl<=31;$tgl++){

													echo '<option value="'.$tgl.'">'.$tgl.'</option>';
											}
											?>
											</select></td>
											<td>
												<select class="form-control" name="bulan">
													<option value="<?php echo $bulan;?>"><?php echo $bulan;?></option>
													<option value="Januari">Januari</option>
													<option value="Februari">Februari</option>
													<option value="Maret">Maret</option>
													<option value="April">April</option>
													<option value="Mei">Mei</option>
													<option value="Juni">Juni</option>
													<option value="Juli">Juli</option>
													<option value="Agustus">Agustus</option>
													<option value="September">September</option>
													<option value="Oktober">Oktober</option>
													<option value="November">November</option>
													<option value="Desember">Desember</option>
												</select>
											</td>
											<td><select class="form-control" name="tahun">
											<option value="<?php echo $tahun;?>"><?php echo $tahun;?></option>
											<?php

											$now = date('Y');
											$last = $now-150;
											for($thn=$now;$thn>=$last;$thn--){

													echo '<option value="'.$thn.'">'.$thn.'</option>';
											}
											?>
											</select></td>
										</tr>
									</table>
								</div>

								<div class="form-group">
									<label>Jenis Kelamin</label>
									<div class="radio">
										<label>
											<input type="radio" name="jk" id="optionsRadios1" value="l" checked>Perempuan
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="jk" id="optionsRadios2" value="p">Laki - laki
									</div>
								</div>

								<div class="form-group">
									<label>Alamat</label>
									<textarea class="form-control" rows="3"></textarea>
								</div>

								<div class="form-group">
									<label>Email </label>
									<input class="form-control" placeholder="Masukkan Email">
								</div>
																
								<div class="form-group">
									<label>Foto</label>
									<input type="file">
									 <!--<p class="help-block">Example block-level help text here.</p>-->
								</div>
								
								<button type="submit" class="btn btn-primary"> Save </button>
								<button type="reset" class="btn btn-default"> Reset </button>
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->