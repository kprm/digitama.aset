		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Upload Asset</h1>
			</div>
		</div><!--/.row-->
				
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-6">
							<form role="form" method="post" action="/aset/updateAset?id=<?php echo $id; ?>" enctype="multipart/form-data"> 

								
							
								<!-- Form untuk mengisi judul-->
								<?php
								$haserror_judul = "";	//memberikan pesan error
								if(isset($error['judul']))
						       	{
						       		$haserror_judul = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_judul; ?>">
									<label>Judul Aset </label>
									<input class="form-control" name="judul" value="<?php echo $judul; ?>">
									<?php
									if(isset($error['judul']))
							       	{
							           foreach($error['judul'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>
								

								<!-- Form untuk mengisi file-->
								<div class="form-group <?php echo $haserror_tipe; ?>">
									<label>Tipe Aset </label>
										<select class="form-control" name="tipe">
											<option value="<?php echo $tipe;?>"><?php echo $tipe;?></option>
											<option value="Picture">Picture</option>
											<option value="Sketch">Sketch</option>
											<option value="Audio">Audio</option>
											<option value="3D">3D Modelling</option>
										</select>		
											
								</div>
								
								<!-- Form untuk mengisi group-->
								<?php
								$haserror_group = "";	//memberikan pesan error
								if(isset($error['group']))
						       	{
						       		$haserror_group = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_group; ?>">
									<label>Group </label>
									<select name="group" class="form-control">
    									<?php
								       	$db = Db::init();
										$col = $db -> group;
										$dt=$col->find();	// memilih semua == select all
										$data = array();
										$data2 = $col -> findone (array("_id" => new mongoid($group)));
										//$nama=$data2['namaGrup'];
										echo '<option value="'.$data2['_id'].'">'.$data2['namaGrup'].'</option>';
										foreach($dt as $dta)
										{
					

										echo "<option value='".$dta['_id']."'> ".$dta['namaGrup']." </option>";

										}
										?>
									</select>
									<?php
									if(isset($error['group']))
							       	{
							           foreach($error['group'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>


								<!-- Form untuk mengisi tgl upload-->
								<?php
								$haserror_timeUpload = "";	//memberikan pesan error
								if(isset($error['timeUpload']))
						       	{
						       		$haserror_timeUpload = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_timeUpload; ?>">
									<!-- <label>Tanggal Upload </label> -->
									<input class="form-control" name="timeUpload" value="<?php echo $timeUpload; ?>" type="hidden">
									<?php
									if(isset($error['timeUpload']))
							       	{
							           foreach($error['timeUpload'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>


								<!-- Form untuk mengisi tgl upload-->
								<?php
								$haserror_lastUpdate = "";	//memberikan pesan error
								if(isset($error['lastUpdate']))
						       	{
						       		$haserror_lastUpdate = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_lastUpdate; ?>">
									<!-- <label>Tanggal Update</label> -->
									<input class="form-control" name="lastUpdate" value="<?php echo date("d-m-Y"); ?>" type="hidden">
									<?php
									if(isset($error['lastUpdate']))
							       	{
							           foreach($error['lastUpdate'] as $err)
							            {
							               echo '<span class="help-block">'.$err.'</span>';	//menampilkan pesan error
							            	/*echo '<div class="alert bg-danger" role="alert">
													<svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kolom Belum diisi <a href="#" class="pull-right"></span></a>
												</div>';*/
							            }
							       	}
									?>
								</div>							


								<!-- Form untuk mengisi foto-->						
								<div class="form-group">
									<label>Aset</label>
									<input type="file" name="file_asli" value="<?php echo $file_asli; ?>">
									 <!--<p class="help-block">Example block-level help text here.</p>-->
								</div>


								<!-- Form untuk mengisi deskripsi-->
								<?php
								$haserror_desk = "";	//memberikan pesan error
								if(isset($error['desk']))
						       	{
						       		$haserror_desk = "has-error";
						       	}
								?>
								<div class="form-group <?php echo $haserror_desk ?>">
									<label>Deskripsi</label>
									<textarea class="form-control" rows="3" name="desk"><?php echo $desk; ?></textarea>
									<?php
									if(isset($error['desk']))
							       	{
							           foreach($error['desk'] as $err)
							            {
							                echo '<span class="help-block">'.$err.'</span>';
							            }
							       	}
									?>
								</div>

								<!-- Form untuk mengisi status-->
								<div class="form-group <?php echo $haserror_status; ?>">
									<label>Status </label>
										<select class="form-control" name="status">
											<option value="<?php echo $status;?>">-<?php echo $status;?>-</option>
											<option value="On Progress">On Progress</option>
											<option value="Fix(Done)">Fix(Done)</option>
										</select>		
								</div>


								

								
								
								<button type="submit" class="btn btn-primary">Submit</button>	<!-- button untuk menyimpan data--> 
								<button type="reset" class="btn btn-default">Reset</button>	
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
		