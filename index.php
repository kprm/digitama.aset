<?php
date_default_timezone_set("Asia/Jakarta");
ini_set('display_errors',1);
ini_set('display_startup_errors',1); 
//ini_set('always_populate_raw_post_data', -1);
error_reporting(-1);
ini_set('memory_limit', '512M');

session_start();
define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('DOCVIEW', DOCROOT."view".DIRECTORY_SEPARATOR);
define('BASE_URL', "http://".$_SERVER['HTTP_HOST']."/");

//include_once(DOCROOT."lib/chilkat/chilkat_9_5_0.php");

function __autoload($class_name) {
	$s = explode("_", $class_name);
	if(count($s) == 1)
	{
		if(file_exists(DOCROOT."/classes/".$class_name . '.php'))
		{
	    	include DOCROOT."/classes/".$class_name . '.php';
		}
		else if(file_exists(DOCROOT."/lib/".$class_name . '.php'))
		{
			include DOCROOT."/lib/".$class_name . '.php';
		}
	}
	else {
		if(file_exists(DOCROOT."/classes/".$s[1]."/".$s[0] . '.php'))
		{
			include DOCROOT."/classes/".$s[1]."/".$s[0] . '.php';
		}
	}
	
}

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
if($path == "/")
{
	$r = new welcome_controller();
	$r->index();
}
else {
	$adacontent = false;
	$pp = explode("/", $path);
	if(count($pp) > 2)
	{	
		if (class_exists($pp[1].'_controller')) {
			$rr = $pp[1].'_controller';
			$r = new $rr();
			$method = $pp[2];
			if (strpos($method,'.') !== false) {
			    $b = explode('.', $method);
			    $method = $b[0];
			}
			if(method_exists($r, $method))
			{
				$r->$method();
				$adacontent = true;
			}
		}
	}
	if(!$adacontent)
	{
		echo "not Found@";
	}
}	
