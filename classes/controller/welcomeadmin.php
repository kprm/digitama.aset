<?php 

class welcomeadmin_controller extends controller 
{
	public function index() {

		$db = Db::init();
		$col = $db -> assets;
		$dt=$col->find( array("tipe" => "Picture"))->limit(8)->sort(array("time_created" => -1));	
		$dt2=$col->find( array("tipe" => "Sketch"))->limit(8)->sort(array("time_created" => -1));	
		$dt3=$col->find( array("tipe" => "Audio"))->limit(8)->sort(array("time_created" => -1));	
		$dt4=$col->find( array("tipe" => "3D Modelling"))->limit(8)->sort(array("time_created" => -1));	
		$dt5=$col->count(array());
		$col2 = $db -> users;
		$dt6=$col2->count(array());
		$col3 = $db -> group;
		$dt7=$col3->count(array());
		$col4 = $db -> project;
		$dt8=$col4->count(array());
		
		$p = array(
			"view_admin" => "",
			"data" => $dt,
			"data2" => $dt2,
			"data3" => $dt3,
			"data4" => $dt4,
			"aset" => $dt5,
			"user" => $dt6,
			"grup" => $dt7,
			"project" => $dt8
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/index.php', $p);


		$p = array(
			"content" => $content 
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	/*public function setting() {
		$p = array(
			"view_admin" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/setting.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}*/




	public function profile() {
		$p = array(
			"view_admin" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/profile.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function viewAllPict() {

		$db = Db::init();
		$col = $db -> assets;
		$dt=$col->find( array("tipe" => "Picture" ))->limit(6)->sort(array("time_created" => -1));	
		
		
			$p = array(
				"view_admin" => "",
				"data" => $dt
				);
		if($_SESSION['status']=="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/allPict.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		} else {
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/allPict.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);

			echo $view;
		}
	}

	public function viewAllSketch() {

		$db = Db::init();
		$col = $db -> assets;
		$dt=$col->find( array("tipe" => "Sketch" ))->limit(6)->sort(array("time_created" => -1));	
		
		$p = array(
			"view_admin" => "",
			"data" => $dt
			);
		if($_SESSION['status']=="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/allSketch.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		} else {
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/allSketch.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);

			echo $view;
		}
	}

	public function viewAllMp3() {

		$db = Db::init();
		$col = $db -> assets;
		$dt=$col->find( array("tipe" => "Audio" ))->limit(6)->sort(array("time_created" => -1));	
		
		$p = array(
			"view_admin" => "",
			"data" => $dt
			);
		if($_SESSION['status']=="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/allMp3.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		} else {
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/allMp3.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);

			echo $view;
		}
	}

	public function viewAll3d() {

		$db = Db::init();
		$col = $db -> assets;
		$dt=$col->find( array("tipe" => "3D Modelling" ))->limit(6)->sort(array("time_created" => -1));	
		
		$p = array(
			"view_admin" => "",
			"data" => $dt
			);
		if($_SESSION['status']=="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/all3d.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		} else {
			$content = $this->getView(DOCVIEW.'welcome_admin/asset/all3d.php', $p);

			$p = array(
				"content" => $content 
				);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);

			echo $view;
		}
	}




	public function view3d() {
		$p = array(
			"view_admin" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/detail3d.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

}