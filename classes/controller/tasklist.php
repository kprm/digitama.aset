<?php 

class taskList_controller extends controller 
{

	public function tasklist() {
		
		$p = array(
			"view_admin" => "",
			
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/tasklist/task_list.php', $p);
 
		$p = array(
			"content" => $content 
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}
	public function tasklistUser() {
		
		$p = array(
			"view_admin" => "",
			
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/tasklist/task_list.php', $p);
 
		$p = array(
			"content" => $content 
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
	}

	
	public function formAddTask() {
		$idProject="";
		$judul="";
		$deskripsi ="";
		$dikerjakanOleh ="";
		$dikerjakanPulaOleh = "";
		$prioritas ="";
		$tglSelesai="";
		$bulanesai="";
		$tahunSelesai="";
		$status="";
		$ket="";

		$error = array();
		//$foto = ""; 
		if (!empty($_POST) ){
			if(isset($_POST ['idProject']))
				$idProject = $_POST['idProject'];
			if(isset($_POST ['judul']))
				$judul = $_POST['judul'];
			if(isset($_POST ['deskripsi']))
				$deskripsi = $_POST['deskripsi'];
			if(isset($_POST ['dikerjakanOleh']))
				$dikerjakanOleh = $_POST['dikerjakanOleh'];
			if(isset($_POST ['dikerjakanPulaOleh']))
				$dikerjakanPulaOleh = $_POST['dikerjakanPulaOleh'];
			if(isset($_POST ['prioritas']))
				$prioritas = $_POST['prioritas']; 
			if(isset($_POST ['tglSelesai']))
				$tglSelesai = $_POST['tglSelesai'];
			if(isset($_POST ['bulanSelesai']))
				$bulanSelesai = $_POST['bulanSelesai'];
			if(isset($_POST ['tahunSelesai']))
				$tahunSelesai = $_POST['tahunSelesai']; 
			if(isset($_POST ['status']))
				$status = $_POST['status'];
			if(isset($_POST ['ket']))
				$ket = $_POST['ket']; 
			
		
			$validator = new Validator();
	        $validator->addRule('idProject', array('require'));
	        $validator->addRule('judul', array('require'));
	        $validator->addRule('deskripsi', array('require'));
	        $validator->addRule('dikerjakanOleh', array('require'));
	        $validator->addRule('dikerjakanPulaOleh', array('require'));
	        $validator->addRule('prioritas', array('require'));
	        $validator->addRule('tglSelesai', array('require'));
	        $validator->addRule('bulanSelesai', array('require'));
	        $validator->addRule('tahunSelesai', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('ket', array('require'));


	       // $validator->addRule('foto', array('require'));
	        $validator->setData(array(
	        	'idProject' => $idProject,
	        	'judul' => $judul,
	        	'deskripsi' => $deskripsi,
	            'dikerjakanOleh' => $dikerjakanOleh,
	            'dikerjakanPulaOleh' => $dikerjakanPulaOleh, 
	            'prioritas' => $prioritas,
	            'tglSelesai' => $tglSelesai,
	            'bulanSelesai' => $bulanSelesai,
	            'tahunSelesai' => $tahunSelesai,
	            'status' => $status,
	            'ket' => $ket,
	
	        ));
	        if($validator->isValid()){
	        	 $db =Db::init();
	        	 $col = $db -> tasklist;
	        	 $p=array( 
	        	 	'idProject' => $idProject,
		        	'judul' => $judul,
		        	'deskripsi' => $deskripsi,
		            'dikerjakanOleh' => $dikerjakanOleh,
		            'dikerjakanPulaOleh' => $dikerjakanPulaOleh, 
		            'prioritas' => $prioritas,
		            'tglSelesai' => $tglSelesai,
	            	'bulanSelesai' => $bulanSelesai,
	            	'tahunSelesai' => $tahunSelesai,
		            'status' => $status,
		            'ket' => $ket,
		
	        	 );
	        	 $col ->insert($p);

	        	if($_SESSION['status']=="Admin"){
		        	header( 'Location: /taskList/tasklist' ) ;
		        	return;
	        	} else if($_SESSION['status']=="User"){
		        	header( 'Location: /taskList/tasklistUser' ) ;
		        	return;
	        	}

	        } else {
	        	$error = $validator->getErrors();
	        }

		}

		$p = array(
			"view_admin" => "",
			'idProject' => $idProject,
        	'judul' => $judul,
        	'deskripsi' => $deskripsi,
            'dikerjakanOleh' => $dikerjakanOleh,
            'dikerjakanPulaOleh' => $dikerjakanPulaOleh, 
            'prioritas' => $prioritas,
            'tglSelesai' => $tglSelesai,
	        'bulanSelesai' => $bulanSelesai,
	        'tahunSelesai' => $tahunSelesai,
            'status' => $status,
            'ket' => $ket,
		    'error' => $error
		);
		if($_SESSION['status']=="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/taskList/formAddTask.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);
		} else if($_SESSION['status']=="User"){
			$content = $this->getView(DOCVIEW.'welcome_admin/taskList/formAddTask.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);
		}
		echo $view;
	}
	
	public function formEditTask() {
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> taskList;	
		$col2 = $db -> users;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{

			$dt2=$col2->findone(array("_id" => new mongoid($dta['dikerjakanOleh'])));
			$p= array(
				"id" => $data['_id'],
				"idProject" => $data['idProject'],
				"judul" => $data['judul'],
		    	"deskripsi" => $data['deskripsi'],
		        "dikerjakanOleh" => $data['dikerjakanOleh'],
		        "dikerjakan"=> $dt2['nama'],
		        "dikerjakanPulaOleh" => $data['dikerjakanPulaOleh'], 
		        "dikerjakaPula"=> $dt2['nama'],
		        "prioritas" => $data['prioritas'],
		        "tglSelesai" => $data['tglSelesai'],
		        "bulanSelesai" => $data['bulanSelesai'],
		        "tahunSelesai" => $data['tahunSelesai'],
		        "status" => $data['status'], 
		        "ket" => $data['ket'],
				
			);
			
			$content = $this->getView(DOCVIEW.'welcome_admin/listTask/formEditTask.php', $p);

			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		}
	}

	public function updateTask() {
 
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> taskList;		//masuk ke colom users
		$data = $col -> find (	//untuk memilih satu / select all
			array(
				"_id" => new mongoid($id)
				)
			);

		if (!empty($_POST) ){
			if(isset($_POST ['idProject']))
				$idProject = $_POST['idProject'];
			if(isset($_POST ['judul']))
				$judul = $_POST['judul'];
			if(isset($_POST ['deskripsi']))
				$deskripsi = $_POST['deskripsi'];
			if(isset($_POST ['dikerjakanOleh']))
				$dikerjakanOleh = $_POST['dikerjakanOleh'];
			if(isset($_POST ['dikerjakanPulaOleh']))
				$dikerjakanPulaOleh = $_POST['dikerjakanPulaOleh'];
			if(isset($_POST ['prioritas']))
				$prioritas = $_POST['prioritas']; 
			if(isset($_POST ['tglSelesai']))
				$tglSelesai = $_POST['tglSelesai'];
			if(isset($_POST ['bulanSelesai']))
				$bulanSelesai = $_POST['bulanSelesai'];
			if(isset($_POST ['tahunSelesai']))
				$tahunSelesai = $_POST['tahunSelesai']; 
			if(isset($_POST ['status']))
				$status = $_POST['status'];
			if(isset($_POST ['ket']))
				$ket = $_POST['ket']; 
			
		
			$validator = new Validator();
	        $validator->addRule('idProject', array('require'));
	        $validator->addRule('judul', array('require'));
	        $validator->addRule('deskripsi', array('require'));
	        $validator->addRule('dikerjakanOleh', array('require'));
	        $validator->addRule('dikerjakanPulaOleh', array('require'));
	        $validator->addRule('prioritas', array('require'));
	        $validator->addRule('tglSelesai', array('require'));
	        $validator->addRule('bulanSelesai', array('require'));
	        $validator->addRule('tahunSelesai', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('ket', array('require'));
	        $validator->setData(array(
	        	'idProject' => $idProject,
	        	'judul' => $judul,
	        	'deskripsi' => $deskripsi,
	            'dikerjakanOleh' => $dikerjakanOleh,
	            'tglSelesai' => $tglSelesai,
	            'bulanSelesai' => $bulanSelesai,
	            'tahunSelesai' => $tahunSelesai,
	            'prioritas' => $prioritas,
	            'tglHarusSelesai' => $tglHarusSelesai,
	            'status' => $status,
	            'ket' => $ket,
	        ));
	        if($validator->isValid()){

		        if($data)

		        	$p=array( 
			        	'idProject' => $idProject,
			        	'judul' => $judul,
			        	'deskripsi' => $deskripsi,
			            'dikerjakanOleh' => $dikerjakanOleh,
			            'tglSelesai' => $tglSelesai,
	            		'bulanSelesai' => $bulanSelesai,
	            		'tahunSelesai' => $tahunSelesai,
			            'prioritas' => $prioritas,
			            'tglHarusSelesai' => $tglHarusSelesai,
			            'status' => $status,
			            'ket' => $ket,
			      
		        	);

	        	
	        	$col->update(array('_id' => new MongoId($id)), array('$set' => $p));

	        	header( 'Location: /taskList/taskList' ) ;
	        	return;
	        }

		} else {
			header( 'Location: /taskList/tasklistUser' ) ;
	        return;
		}

		$p = array(
			"view_admin" => "",
			'idProject' => $idProject,
        	'judul' => $judul,
        	'deskripsi' => $deskripsi,
            'dikerjakanOleh' => $dikerjakanOleh,
            'dikerjakanPulaOleh' => $dikerjakanPulaOleh, 
            'prioritas' => $prioritas,
            'tglSelesai' => $tglSelesai,
	        'bulanSelesai' => $bulanSelesai,
	        'tahunSelesai' => $tahunSelesai,
            'status' => $status,
            'ket' => $ket 
		);
		$content = $this->getView(DOCVIEW.'welcome_admin/taskList/formAddList.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function hapusTask() {
		if(!empty($_POST)){
			$id = $_POST['id'];
			
			$db = Db::init();	
			$col = $db -> taskList;		//masuk ke colom users
			$data = $col -> findone (	//untuk memilih satu
				array(
					"_id" => new mongoid($id)
					)
				);
			if(isset($data['_id'])){
				// $pathFile = './public/fotoUser/'.$data['foto'];
				// if(file_exists($pathFile)){
				// 	unlink($pathFile);
				// }

				$p = array('_id' => new Mongoid($id));
				
				$col -> remove($p);

		        	$p=array(
						"hasil" => "OK"
					);
				echo json_encode($p); 
				return;
			}

		}
		$p=array(
			"hasil" => "ERROR"
		);
			echo json_encode($p);
	}



	public function ambildata(){
		$db = Db::init();
		$col = $db -> taskList;
		$col2 = $db -> project;
		$col3 = $db -> users;
		$dt=$col->find();	// memilih semua == select all
		$data = array();
		foreach($dt as $dta)
		{
			$dt2=$col2->findone(array("_id" => new mongoid($dta['idProject'])));
			$dt3=$col3->findone(array("_id" => new mongoid($dta['dikerjakanOleh'])));
			$dt4=$col3->findone(array("_id" => new mongoid($dta['dikerjakanPulaOleh'])));

			
			$p= array(
				"id" => trim($dta['_id']),
				"idProject" => $dta['idProject'],
				"project" => $dt2['namaProject'],
				"judul" => $dta['judul'],
				"deskripsi" => $dta['deskripsi'],
				"dikerjakanOleh" => $dta['dikerjakanOleh'],				
				"dikerjakan" => $dt3['nama'],
				"dikerjakanPulaOleh" => $dta['dikerjakanPulaOleh'],
				"dikerjakanPula" => $dt4['nama'],
				"prioritas" => $dta['prioritas'],
				"tglHarusSelesai" => $dta['tglSelesai'].'-'.$dta['bulanSelesai'].'-'.$dta['tahunSelesai'],
				"status" => $dta['status'],
				"ket" => $dta['ket'],
				"namaProject" => $dt2['namaProject']
			);
			$data[]= $p;
		}

		echo json_encode($data);
	}


	
}