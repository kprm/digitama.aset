<?php 

class welcome_controller extends controller 
{
	
	public function index() {

		// $db = Db::init();	
		// $col = $db -> users;		//masuk ke colom users
		// $data = $col -> findone (	//untuk memilih satu
		// 	array(
		// 		"id" => new mongoid($id)
		// 	)
		// );

		// if(isset($data['_id'])) // artinya data ada 
		// {

		if($_SESSION['status']=="User"){

			$db = Db::init();
			$col = $db -> assets;
			$dt=$col->find( array("tipe" => "Picture"))->limit(8)->sort(array("time_created" => -1));	
			$dt2=$col->find( array("tipe" => "Sketch"))->limit(8)->sort(array("time_created" => -1));	
			$dt3=$col->find( array("tipe" => "Audio"))->limit(8)->sort(array("time_created" => -1));	
			$dt4=$col->find( array("tipe" => "3D Modelling"))->limit(8)->sort(array("time_created" => -1));	
			$dt5=$col->count(array());
			$col2 = $db -> users;
			$dt6=$col2->count(array());
			$col3 = $db -> group;
			$dt7=$col3->count(array());
			$col4 = $db -> project;
			$dt8=$col4->count(array());
		
			$p = array(
				"view_admin" => "",
				"data" => $dt,
				"data2" => $dt2,
				"data3" => $dt3,
				"data4" => $dt4,
				"aset" => $dt5,
				"user" => $dt6,
				"grup" => $dt7,
				"project" => $dt8
				);
				
			$content = $this->getView(DOCVIEW.'welcome/index.php', $p);
			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);
			echo $view;
				
		} else if($_SESSION['status']=="Admin"){
			
			$db = Db::init();
			$col = $db -> assets;
			$dt=$col->find( array("tipe" => "Picture"))->limit(6)->sort(array("time_created" => -1));	
			$dt2=$col->find( array("tipe" => "Sketch"))->limit(6)->sort(array("time_created" => -1));	
			$dt3=$col->find( array("tipe" => "Audio"))->limit(6)->sort(array("time_created" => -1));	
			$dt5=$col->count(array());
			$col2 = $db -> users;
			$dt6=$col2->count(array());
			$col3 = $db -> group;
			$dt7=$col3->count(array());
			$col4 = $db -> project;
			$dt8=$col4->count(array());$dt4=$col->find( array("tipe" => "3D Modelling"))->limit(6)->sort(array("time_created" => -1));	
			
			$p = array(
				"view_admin" => "",
				"data" => $dt,
				"data2" => $dt2,
				"data3" => $dt3,
				"data4" => $dt4,
				"aset" => $dt5,
				"user" => $dt6,
				"grup" => $dt7,
				"project" => $dt8
				
			);
	
			$content = $this->getView(DOCVIEW.'welcome_admin/index.php', $p);
			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);
			echo $view;
		} 
		//}
	}

	public function assets() {
		$p = array(
			"view_user" => ""
			);
		
		$content = $this->getView(DOCVIEW.'welcome/assets.php', $p);
		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);
		echo $view;

	}

	/*public function upload() {
		if (!empty($_POST) ){
				$nama = $_POST['nama']; echo $nama;
				$kategori = $_POST['kategori']; echo $kategori;
				$asset = $_POST['asset']; echo $asset;
				exit;
		}

		$p = array(
			"view_user" => ""
			);
		
		$content = $this->getView(DOCVIEW.'welcome/upload.php', $p);
		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);
		echo $view;

	}

	public function setting() {
		$p = array(
			"view_user" => ""
			);
		
		$content = $this->getView(DOCVIEW.'welcome/setting.php', $p);
		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);
		echo $view;

	}

	public function profile() {
		$p = array(
			"view_user" => ""
			);
		
		$content = $this->getView(DOCVIEW.'welcome/profile.php', $p);
		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);
		echo $view;

	}

	public function detPictUser() {
		$p = array(
			"view_user" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome/detPictUser.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
	}

	public function detMp3User() {
		$p = array(
			"view_user" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome/detMp3User.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
	}

	public function det3dUser() {
		$p = array(
			"view_user" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome/det3dUser.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
	}

	public function detSketsaUser() {
		$p = array(
			"view_user" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome/detSketsaUser.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
	}*/

}
