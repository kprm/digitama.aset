<?php 

class user_controller extends controller 
{
	

	public function listuser() { 
		$p = array(
			"view_admin" => "" 
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/user/list_user.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	
	public function formAddUser() {
		$nama = "";
		$tanggal = ""; 
		$bulan = ""; 
		$tahun = ""; 
		$jk = ""; 
		$alamat = ""; 
		$email = ""; 
		$telepon = ""; 
		$pass = ""; 
		$foto_asli = ""; 
		$foto_rename = ""; 
		$status= "";
		$bagian= "";
		$error = array();
		if (!empty($_POST) ){
			if(isset($_POST ['nama']))
				$nama = $_POST['nama'];
			if(isset($_POST ['tanggal']))
				$tanggal = $_POST['tanggal']; 
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan']; 
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			if(isset($_POST ['jk']))
				$jk = $_POST['jk']; 
			if(isset($_POST ['alamat']))
				$alamat = $_POST['alamat']; 
			if(isset($_POST ['email']))
				$email = $_POST['email']; 
			if(isset($_POST ['telepon']))
				$telepon = $_POST['telepon']; 
			if(isset($_POST ['pass']))
				$pass = $_POST['pass']; 
			if(isset($_POST ['status']))
				$status = $_POST['status'];  
			if(isset($_POST ['bagian']))
				$bagian = $_POST['bagian']; 
			//if(isset($_POST ['foto']))
				//$foto = $_POST['foto']; 
		
			$validator = new Validator();
	        $validator->addRule('nama', array('require'));
	        $validator->addRule('tanggal', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('jk', array('require'));
	        $validator->addRule('alamat', array('require'));
	        $validator->addRule('email', array('email', 'require'));
	        $validator->addRule('pass', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('telepon', array('numeric','require'));
	        $validator->addRule('bagian', array('numeric','require'));
	        //$validator->addRule('foto', array('require'));
	        $validator->setData(array(
	        	'nama' => $nama,
	            'tanggal' => $tanggal,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'jk' => $jk,
	            'alamat'=> $alamat,
	            'email' => $email,
	            'telepon' => $telepon,
	            'pass' =>$pass,
	            // 'foto_asli' => $foto_asli,
	            'status' => $status,
	            'bagian' => $bagian,
	        ));
	        if($validator->isValid()){

	   //     		$tipe_gambar = array('image/jpeg','image/bmp', 'image/png');
				// $foto = $_FILES['foto_asli']['name'];
				// $ukuran = $_FILES['foto_asli']['size'];
				// $tipe = $_FILES['foto_asli']['type'];

				// if($foto!=" " && $ukuran> 0){
				// if(in_array(strtolower($tipe), $tipe_gambar)){
				// //move_uploaded_file($_FILES['foto']['tmp_name'], './public/fotoUser/'.$foto);	
				// $temp = explode(".", $_FILES['foto_asli']['name']);
				// $newfilename = round(microtime(true)) . '.' . end($temp);
				// move_uploaded_file($_FILES['foto_asli']['tmp_name'], './public/fotoUser/' . $newfilename);
			
	        	 $db =Db::init();
	        	 $col = $db -> users;
	        	 $p=array( 
		        	'nama' => $nama,
		            'tanggal' => $tanggal,
		            'bulan' => $bulan,
		            'tahun' => $tahun,
		            'jk' => $jk,
		            'alamat'=> $alamat,
		            'email' => $email,
		            'telepon' => $telepon,
		            'pass' =>$pass,
		            'foto_rename' => "default.png",
		            'foto_asli' => "default.png",
		            'status' => $status,
		            'bagian' => $bagian,


	        	 );
	        	 $col ->insert($p);

	        	$col2 = $db -> auditTrail;
			    $data2 = $col -> findone (	//untuk memilih satu
					array(
						'nama' => $nama,
						'status' => $status,
		            	'bagian' => $bagian,
						)
					);
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Add User",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $data2['_id'],
	
	        	);
	        	$col2 ->insert($p2);

	        	header( 'Location: /user/listuser' ) ;
	        	return;
	        }
	  		 // }}
	        else
	        {
	        	$error = $validator->getErrors();
	        }
		}

		$p = array(
			"view_admin" => "",
			'nama' => $nama,
		            'tanggal' => $tanggal,
		            'bulan' => $bulan,
		            'tahun' => $tahun,
		            'jk' => $jk,
		            'alamat'=> $alamat,
		            'telepon' => $telepon,
		            'email' => $email,
		            'pass' =>$pass,
		            'foto_asli' => $foto_asli,
		            'foto_rename' => $foto_rename,
		            'status' => $status,
		            'bagian' => $bagian,
		            'error' => $error,
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/user/formAddUser.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}
	
	

	public function formEditUser() {
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{
			$p= array(
				"id" => $data['_id'],
				"nama" => $data['nama'],
				"tanggal" => $data['tanggal'],
				"bulan" => $data['bulan'],
				"tahun" => $data['tahun'],
				"jk" => $data['jk'],
				"alamat" => $data['alamat'],
				"telepon" => $data['telepon'],
				"email" => $data['email'],
				//"pass" => $data['pass'],
				//"foto_asli" => $data['foto_asli'],
				"status" => $data['status'],
				"bagian" => $data['bagian'],
			);

			$content = $this->getView(DOCVIEW.'welcome_admin/user/formEditUser.php', $p);

			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		}
		//echo json_encode($p);
	}

	public function updateUser() {

		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> find (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if (!empty($_POST) ){
			if(isset($_POST ['nama']))
				$nama = $_POST['nama'];
			if(isset($_POST ['tanggal']))
				$tanggal = $_POST['tanggal']; 
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan']; 
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			if(isset($_POST ['jk']))
				$jk = $_POST['jk']; 
			if(isset($_POST ['alamat']))
				$alamat = $_POST['alamat']; 
			if(isset($_POST ['email']))
				$email = $_POST['email'];
			if(isset($_POST ['telepon']))
				$telepon = $_POST['telepon']; 
			// if(isset($_POST ['pass']))
			// 	$pass = $_POST['pass']; 
			// if(isset($_POST ['foto_asli']))
			// 	$foto_asli = $_POST['foto_asli']; 
			if(isset($_POST ['bagian']))
				$bagian = $_POST['bagian']; 
		
			$validator = new Validator();
	        $validator->addRule('nama', array('require'));
	        $validator->addRule('tanggal', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('jk', array('require'));
	        $validator->addRule('alamat', array('require'));
	        $validator->addRule('email', array('require'));
	        $validator->addRule('telepon', array('numeric', 'require'));
	       // $validator->addRule('pass', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('bagian', array('require'));
	       //	$validator->addRule('foto_asli', array('require'));
	        $validator->setData(array(
	        	'nama' => $nama,
	            'tanggal' => $tanggal,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'jk' => $jk,
	            'alamat'=> $alamat,
	            'email' => $email,
	            'telepon' =>$telepon,
	            //'pass' =>$pass,
//	            'foto_asli' => $foto_asli,
	            'status' => $status,
	            'bagian' => $bagian
	        ));
	        if($validator->isValid()){
	        	
	   //      	$tipe_gambar = array('image/jpeg','image/bmp', 'image/png');
				// $foto = $_FILES['foto_asli']['name'];
				// $ukuran = $_FILES['foto_asli']['size'];
				// $tipe = $_FILES['foto_asli']['type'];

				// if($foto!=" " && $ukuran> 0){
				// 	if(in_array(strtolower($tipe), $tipe_gambar)){
				// //move_uploaded_file($_FILES['foto']['tmp_name'], './public/fotoUser/'.$foto);	
				// 		$temp = explode(".", $_FILES['foto_asli']['name']);
				// 		$newfilename = round(microtime(true)) . '.' . end($temp);
				// 		move_uploaded_file($_FILES['foto_asli']['tmp_name'], './public/fotoUser/' . $newfilename);
	        			
	        			if($data && $newfilename) {

	        				 $p=array( 
		        				'nama' => $nama,
					            'tanggal' => $tanggal,
					            'bulan' => $bulan,
					            'tahun' => $tahun,
					            'jk' => $jk,
					            'alamat'=> $alamat,
					            'email' => $email,
					            'telepon' => $telepon,
					           // 'pass' =>$pass,
					           // 'foto_asli' => $foto,
					            'foto_rename' => $newfilename,
					            'status' => $status,
					            'bagian' => $bagian
	        	 			);

	        			} else {
	        					$p=array( 
		        				'nama' => $nama,
					            'tanggal' => $tanggal,
					            'bulan' => $bulan,
					            'tahun' => $tahun,
					            'jk' => $jk,
					            'alamat'=> $alamat,
					            'email' => $email,
					            'telepon' => $telepon,
					            //'pass' =>$pass,
					            'status' => $status,
					            'bagian' => $bagian,
	        	 			);
	        			}
	        	
	        	 		$col->update(array('_id' => new MongoId($id)), array('$set' => $p));
	        	 		$col2 = $db -> auditTrail;
					    
					    $p2=array(
					        'idUser' => $_SESSION['id'],
				        	'aksi' => "Update User",
				        	'tgl' => date("d-m-Y"),
				        	'jam' => date("H:i:s"),
				            'ket' => $id,

			        	);
			        	$col2 ->insert($p2);


	        			header( 'Location: /user/listuser' ) ;
	        			return;
	       		// 	}
	       		// }
	       	}
		} else {
			header( 'Location: /user/formEditUser' ) ;
	        	return;
		}

		$p = array(
			"view_admin" => "",
					'nama' => $nama,
		            'tanggal' => $tanggal,
		            'bulan' => $bulan,
		            'tahun' => $tahun,
		            'jk' => $jk,
		            'alamat'=> $alamat,
		            'email' => $email,
		            'telepon' => $telepon,
//		            'foto_asli' => $foto_asli,
		            //'foto_rename' => $foto_rename,
		            //'pass' =>$pass,
		            'status' =>$status,
		            'bagian' =>$bagian,
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/user/formEditUser.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;

		
	}

	public function hapusUser() {
		if(!empty($_POST)){
		$id = $_POST['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);
		if(isset($data['_id'])){
			$pathFile = './public/fotoUser/'.$data['foto_rename'];
			if(file_exists($pathFile)){
				unlink($pathFile);
			}

			$p = array('_id' => new Mongoid($id));
			
			$col -> remove($p);

	        	$p=array(
					"hasil" => "OK"
				);
			echo json_encode($p); 

			$col2 = $db -> auditTrail;
			  
		    $p2=array(
		        'idUser' => $_SESSION['id'],
	        	'aksi' => "Delete User",
	        	'tgl' => date("d-m-Y"),
	        	'jam' => date("H:i:s"),
	            'ket' => $id,

        	);
        	$col2 ->insert($p2);
			return;
		}

		}
		$p=array(
					"hasil" => "ERROR"
				);
			echo json_encode($p);

	}

	public function ambildata(){
		$db = Db::init();
		$col = $db -> users;
		$dt=$col->find();	// memilih semua == select all
		$data = array();
		foreach($dt as $dta)
		{
			
			$p= array(
				"id" => trim($dta['_id']),
				"nama" => $dta['nama'],
				"tanggallahir" => $dta['tanggal'].'-'.$dta['bulan'].'-'.$dta['tahun'],
				"jk" => $dta['jk'],
				"alamat" => $dta['alamat'],
				"telepon" => $dta['telepon'],
				"email" => $dta['email'],
				"foto_asli" => $dta['foto_asli'],
				"foto_rename" => $dta['foto_rename'],
				"pass" => $dta['pass'],
				"status" => $dta['status'],
				"bagian" => $dta['bagian'],
				//"bagian" => $dta['bagian']
			);
			$data[]= $p;
		}
		echo json_encode($data);
	}
} 