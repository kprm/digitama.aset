<?php 

class group_controller extends controller 
{

	public function listgroup() {
		
		$p = array(
			"view_admin" => "",
			
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/group/list_group.php', $p);

		$p = array(
			"content" => $content 
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function listgroupuser() {
		$id = $_GET['id'];
		$db = Db::init();
		$col = $db -> group;
		$dt=$col->find( array('idProject' => $id))->sort(array("time_created" => -1));	
		
		$p = array(
			"view_admin" => "",
			"data" => $dt,
		);
		$content = $this->getView(DOCVIEW.'welcome/group.php', $p);

		$p = array(
			"content" => $content 
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
	}
	
	public function formAddGroup() {
		$namaGrup = "";
		$idProject = ""; 
		$tgl = ""; 
		$bulan = ""; 
		$tahun = ""; 
		$status = ""; 
		$ket = ""; 
		$error = array();
		//$foto = ""; 
		if (!empty($_POST) ){
			if(isset($_POST ['namaGrup']))
				$namaGrup = $_POST['namaGrup'];
			if(isset($_POST ['idProject']))
				$idProject = $_POST['idProject'];
			if(isset($_POST ['tgl']))
				$tgl = $_POST['tgl'];
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan'];
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			if(isset($_POST ['status']))
				$status = $_POST['status'];
			if(isset($_POST ['ket']))
				$ket = $_POST['ket']; 
			
		
			$validator = new Validator();
	        $validator->addRule('namaGrup', array('require'));
	        $validator->addRule('idProject', array('require'));
	        $validator->addRule('tgl', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('ket', array('require'));


	       // $validator->addRule('foto', array('require'));
	        $validator->setData(array(
	        	'namaGrup' => $namaGrup,
	        	'idProject' => $idProject,
	            'tgl' => $tgl,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'status' => $status,
	            'ket' => $ket,
	
	        ));
	        if($validator->isValid()){
				$db =Db::init();
				$col = $db -> group;
				$p=array( 
				'namaGrup' => $namaGrup,
				'idProject' => $idProject,
				'tgl' => $tgl,
				'bulan' => $bulan,
				'tahun' => $tahun,
				'status' => $status,
				'ket' => $ket,

				);
				$col ->insert($p);

				$col2 = $db -> auditTrail;
			    $data2 = $col -> findone (	//untuk memilih satu
					array(
						'namaGrup' => $namaGrup,
						'idProject' => $idProject,
						)
					);
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Add Group",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $data2['_id'],
	
	        	);
	        	$col2 ->insert($p2);

	        	if($_SESSION['status']=="Admin"){
		        	header( 'Location: /group/listgroup' ) ;
		        	return;
	        	} else if($_SESSION['status']=="User"){
		        	header( 'Location: /group/listgroupuser' ) ;
		        	return;
	        	}

	        } else {
	        	$error = $validator->getErrors();
	        }

		}

		$p = array(
			"view_admin" => "",
			'namaGrup' => $namaGrup,
			'idProject' => $idProject,
		    'tgl' => $tgl,
		    'bulan' => $bulan,
		    'tahun' => $tahun,
		    'status' => $status,
		    'ket' => $ket,
		    'error' => $error
		);
		if($_SESSION['status']=="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/group/formAddGroup.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);
		} else if($_SESSION['status']=="User"){
			$content = $this->getView(DOCVIEW.'welcome_admin/group/formAddGroup.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);
		}
		echo $view;
	}
	
	public function formEditGroup() {
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> group;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{
			$p= array(
				"id" => $data['_id'],
				"namaGrup" => $data['namaGrup'],
				"idProject" => $data['idProject'],
				"tgl" => $data['tgl'],
				"bulan" => $data['bulan'],
				"tahun" => $data['tahun'],
				"status" => $data['status'],
				"ket" => $data['ket']
			
			);
			
			$content = $this->getView(DOCVIEW.'welcome_admin/group/formEditGroup.php', $p);

			
			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		}
	}

	public function updateGroup() {
 
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> group;		//masuk ke colom users
		$data = $col -> find (	//untuk memilih satu / select all
			array(
				"_id" => new mongoid($id)
				)
			);

		if (!empty($_POST) ){
			if(isset($_POST ['namaGrup']))
				$namaGrup = $_POST['namaGrup'];
			if(isset($_POST ['idProject']))
				$idProject = $_POST['idProject']; 
			if(isset($_POST ['tgl']))
				$tgl = $_POST['tgl']; 
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan']; 
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			if(isset($_POST ['status']))
				$status = $_POST['status']; 
			if(isset($_POST ['ket']))
				$ket = $_POST['ket']; 
			
		
			$validator = new Validator();
	        $validator->addRule('namaGrup', array('require'));
	        $validator->addRule('idProject', array('require'));
	        $validator->addRule('tgl', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('ket', array('require'));
	        $validator->setData(array(
	        	'namaGrup' => $namaGrup,
	            'idProject' => $idProject,
	            'tgl' => $tgl,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'status' => $status,
	            'ket' => $ket
	        ));
	        if($validator->isValid()){

		        if($data)

		        	$p=array( 
			        	'namaGrup' => $namaGrup,
			            'idProject' => $idProject,
			            'tgl' => $tgl,
			            'bulan' => $bulan,
			      		'tahun' => $tahun,
			      		'status' => $status,
			      		'ket' => $ket
			      
		        	);

	        	
	        	$col->update(array('_id' => new MongoId($id)), array('$set' => $p));

	        	$col2 = $db -> auditTrail;
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Update Group",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $id,
	
	        	);
	        	$col2 ->insert($p2);


	        	header( 'Location: /group/listgroup' ) ;
	        	return;
	        }

		} else {
			header( 'Location: /group/formEditGroup' ) ;
	        return;
		}

		$p = array(
			"view_admin" => "",
			'namaGrup' => $namaGrup,
            'idProject' => $idProject,
            'tgl' => $tgl,
            'bulan' => $bulan,
            'tahun' => $tahun,
            'status' => $status,
            'ket' => $ket
		);
		$content = $this->getView(DOCVIEW.'welcome_admin/group/formAddGroup.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function hapusGroup() {
		if(!empty($_POST)){
			$id = $_POST['id'];
			
			$db = Db::init();	
			$col = $db -> group;		//masuk ke colom users
			$data = $col -> findone (	//untuk memilih satu
				array(
					"_id" => new mongoid($id)
					)
				);
			if(isset($data['_id'])){
				// $pathFile = './public/fotoUser/'.$data['foto'];
				// if(file_exists($pathFile)){
				// 	unlink($pathFile);
				// }

				$p = array('_id' => new Mongoid($id));
				
				$col -> remove($p);

		        	$p=array(
						"hasil" => "OK"
					);
				echo json_encode($p); 

				$col2 = $db -> auditTrail;
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Delete Group",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $id,
	
	        	);
	        	$col2 ->insert($p2);
				return;
			}

		}
		$p=array(
			"hasil" => "ERROR"
		);
			echo json_encode($p);
	}



	public function ambildataGroup(){
		$db = Db::init();
		$col = $db -> group;
		$col2 = $db -> project;
		$dt=$col->find();	// memilih semua == select all
		$data = array();
		foreach($dt as $dta)
		{
			$dt2=$col2->findone(array("_id" => new mongoid($dta['idProject'])));
			
			$p= array(
				"id" => trim($dta['_id']),
				"namaGrup" => $dta['namaGrup'],
				"idProject" => $dta['idProject'],
				"tglMulai" => $dta['tgl'].'-'.$dta['bulan'].'-'.$dta['tahun'],
				"status" => $dta['status'],
				"ket" => $dta['ket'],
				"namaProject" => $dt2['namaProject']
			);
			$data[]= $p;
		}

		echo json_encode($data);
	}


	public function detail(){
		$id =$_GET['id'];
		$idGroup="";
	
		$db = Db::init();	
		$col = $db -> group;
		$col2 = $db -> assets;
		$data = $col -> findone (	
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id']))
		{
		
			$data2=$col2->find( array("group" => $id))->limit(12)->sort(array("time_created" => -1));	
	
			$p= array(
				"id" => trim($data['_id']),
				"namaGroup" => $data['namaGrup'],
				"idProject" => $data['idProject'],
				"tgl" => $data['tgl'],
				"bulan" => $data['bulan'],
				"tahun" => $data['tahun'],
				"tglMulai" => $data['tgl'].'-'.$data['bulan'].'-'.$data['tahun'],
				"status" => $data['status'],
				"ket" => $data['ket'],
				"data" => $data2,
				
				

			);

			
			$content = $this->getView(DOCVIEW.'welcome_admin/detailgroup.php', $p);

			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);

			echo $view;
		}
	}
}