<?php 

class login_controller extends controller 
{
	public function index() {

		$email = "";
		$pass = "";
		$message = "";
		if(!empty($_POST))
		{
			if(isset($_POST ['email']))
				$email = $_POST ['email'];
			if(isset($_POST ['password']))
				$pass = $_POST ['password'];
			
			$validator = new Validator();
	        $validator->addRule('email', array('email', 'require'));
	        $validator->addRule('pass', array('require'));
	        $validator->setData(array(
	        	'email' => $email,
	            'pass' => $pass
	        ));
 
	        if($validator->isValid()){

	       		$db = Db::init();	
				$col = $db -> users;		//masuk ke colom users
				$data = $col -> findone (	//untuk memilih satu
					array(
						"email" => $email,
						"pass" => $pass,
						//"_id" => new mongoid($id)
					)
				);

				if(isset($data['_id'])) // artinya data ada 
				{
					//LOGIN OK ATAU DATA ADA
				

				//DATA GAK ADA / LOGIN FAILED
				//if($email=='user@gmail.com' && $pass=='123'){
	        	 	if($data['status']=='User'){
				
						$_SESSION['email']=$email;
						$_SESSION['id']=trim($data['_id']);
						$_SESSION['nama']=$data['nama'];
						$_SESSION['status']=$data['status'];

						$col2 = $db -> auditTrail;
	        			$p=array( 
			        	'idUser' => $_SESSION['id'],
			        	'aksi' => "Login",
			        	'tgl' => date("d-m-Y"),
			        	'jam' => date("H:i:s"),
			            'ket' => "-",
			
			        	);
			        	$col2 ->insert($p);
				
						header( 'Location: /welcome/index' ) ;
						return;

					}else if($data['status']=='Admin'){
					// } else if($email=="email" && "status"=='Admin'){
					
						$_SESSION['email']=$email;
						$_SESSION['id']=trim($data['_id']);
						$_SESSION['nama']=$data['nama'];
						$_SESSION['status']=$data['status'];

						$col2 = $db -> auditTrail;
	        			$p=array( 
				        	'idUser' => $_SESSION['id'],
				        	'aksi' => "Login",
				        	'tgl' => date("d-m-Y"),
				        	'jam' => date("H:i:s"),
				            'ket' => "-",
			
			        	);
			        	$col2 ->insert($p);
				
						header( 'Location: /welcomeadmin/index' ) ;
						return;
					} else {
						 $message = "gagal login, email salah";
					}

				}
	        } else {
	        	 $message = "harus diisi";
	        }
		}

		$curl = new Curl();
    	$curl->post('http://sso.deboxs.com/api/akun/get_url_facebook_login', array(
		    'clientid' => '575e6d5412959525308b456c'
		));

		$h = $curl->response;
		if(!is_object($h))
			$h = json_decode($h);

		$urlfacebook = "";

		if(isset($h->url))
			$urlfacebook = $h->url;

		$curl->post('http://sso.deboxs.com/api/akun/get_url_google_login', array(
			'clientid' => '575e6d5412959525308b456c'
		));

		$h = $curl->response;

		if(!is_object($h))
			$h = json_decode($h);
		
		$urlgoogle = "";

		if(isset($h->url))
			$urlgoogle = $h->url;

		$p = array(
			"urlfacebook" => $urlfacebook,
			"urlgoogle" => $urlgoogle,
			"email" => $email,
			"message" => $message
		);
		
		$view = $this->getView(DOCVIEW.'login/login.php', $p);
		echo $view;

	}

	public function logout(){
		$db = Db::init();
		$col2 = $db -> auditTrail;
		$p=array( 
        	'idUser' => $_SESSION['id'],
        	'aksi' => "Logout",
        	'tgl' => date("d-m-Y"),
        	'jam' => date("H:i:s"),
            'ket' => "-",

    	);
    	$col2 ->insert($p);
		session_destroy();
		header( 'Location: /login/index' ) ;
	}

	public function callback()
	{
		$userid = "";
		if(isset($_GET['userid']))
			$userid = $_GET['userid'];

		$token = "";

		if(isset($_GET['token']))
			$token = $_GET['token'];

		$clientid = '575e6d5412959525308b456c';
		
		if((strlen(trim($userid)) > 0) && (strlen(trim($token)) > 0))
		{
			$curl = new Curl();
	    	$curl->post('http://sso.deboxs.com/api/akun/get_user', array(
			    	'clientid' => $clientid,
			    	'token' => $token,
			    	'userid' => $userid
			));
			$h = $curl->response;

			if(!is_object($h))
				$h = json_decode($h);

			if(isset($h->_id))
			{
				$db = Db::init();
				$usr = $db->users;
				$data = $usr->findone(array('email' => $h->email));

				if(isset($data['_id']))
				{
					// $dausr = $usr->findone(array('email' => $h->email));
					// 	$usrid = trim($dausr['_id']);	
					if($data['status']=='User'){
				
						$_SESSION['email']=$email;
						$_SESSION['id']=trim($data['_id']);
						$_SESSION['nama']=$data['nama'];
						$_SESSION['status']=$data['status'];
				
						header( 'Location: /welcome/index' ) ;
						return;

					}else if($data['status']=='Admin'){
					// } else if($email=="email" && "status"=='Admin'){
					
						$_SESSION['email']=$email;
						$_SESSION['id']=trim($data['_id']);
						$_SESSION['nama']=$data['nama'];
						$_SESSION['status']=$data['status'];
				
						header( 'Location: /welcomeadmin/index' ) ;
						return;
					}
					return;
				} else {
					echo '<h2>Sorry, you not authorized to view this page</h2>';
					return;
				}
			}
		}
	}

}