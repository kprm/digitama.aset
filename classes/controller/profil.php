<?php 

class profil_controller extends controller 
{
	

	/*public function listuser() { 
		$p = array(
			"view_admin" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/user/list_user.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	
	public function formAddUser() {
		$nama = "";
		$tanggal = ""; 
		$bulan = ""; 
		$tahun = ""; 
		$jk = ""; 
		$alamat = ""; 
		$email = ""; 
		$pass = ""; 
		$foto = ""; 
		$status= "";
		$error = array();
		if (!empty($_POST) ){
			if(isset($_POST ['nama']))
				$nama = $_POST['nama'];
			if(isset($_POST ['tanggal']))
				$tanggal = $_POST['tanggal']; 
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan']; 
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			if(isset($_POST ['jk']))
				$jk = $_POST['jk']; 
			if(isset($_POST ['alamat']))
				$alamat = $_POST['alamat']; 
			if(isset($_POST ['email']))
				$email = $_POST['email']; 
			if(isset($_POST ['pass']))
				$pass = $_POST['pass']; 
			if(isset($_POST ['status']))
				$status = $_POST['status']; 
			//if(isset($_POST ['foto']))
				//$foto = $_POST['foto']; 
		
			$validator = new Validator();
	        $validator->addRule('nama', array('require'));
	        $validator->addRule('tanggal', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('jk', array('require'));
	        $validator->addRule('alamat', array('require'));
	        $validator->addRule('email', array('require'));
	        $validator->addRule('pass', array('require'));
	        $validator->addRule('status', array('require'));
	        //$validator->addRule('foto', array('require'));
	        $validator->setData(array(
	        	'nama' => $nama,
	            'tanggal' => $tanggal,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'jk' => $jk,
	            'alamat'=> $alamat,
	            'email' => $email,
	            'pass' =>$pass,
	            'foto' => $foto,
	            'status' => $status,
	        ));
	        if($validator->isValid()){

	       		$tipe_gambar = array('image/jpeg','image/bmp', 'image/png');
				$foto = $_FILES['foto']['name'];
				$ukuran = $_FILES['foto']['size'];
				$tipe = $_FILES['foto']['type'];

				if($foto!=" " && $ukuran> 0){
				if(in_array(strtolower($tipe), $tipe_gambar)){
				move_uploaded_file($_FILES['foto']['tmp_name'], './public/fotoUser/'.$foto);	
				
			
	        	 $db =Db::init();
	        	 $col = $db -> users;
	        	 $p=array( 
		        	'nama' => $nama,
		            'tanggal' => $tanggal,
		            'bulan' => $bulan,
		            'tahun' => $tahun,
		            'jk' => $jk,
		            'alamat'=> $alamat,
		            'email' => $email,
		            'pass' =>$pass,
		            'foto' => $foto,
		            'status' => $status,


	        	 );
	        	 $col ->insert($p);

	        	header( 'Location: /user/listuser' ) ;
	        	return;
	        }
	  		 }}
	        else
	        {
	        	$error = $validator->getErrors();
	        }
		}

		$p = array(
			"view_admin" => "",
			'nama' => $nama,
		            'tanggal' => $tanggal,
		            'bulan' => $bulan,
		            'tahun' => $tahun,
		            'jk' => $jk,
		            'alamat'=> $alamat,
		            'email' => $email,
		            'pass' =>$pass,
		            'foto' => $foto,
		            'status' => $status,
		            'error' => $error,
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/user/formAddUser.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}*/
	
	

	public function formSetting() {
		$id =$_SESSION['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{
			$p= array(
				"id" => $data['_id'],
				"nama" => $data['nama'],
				"tanggal" => $data['tanggal'],
				"bulan" => $data['bulan'],
				"tahun" => $data['tahun'],
				"jk" => $data['jk'],
				"alamat" => $data['alamat'],
				"email" => $data['email'],
				"foto_asli" => $data['foto_asli'],
				"pass" => $data['pass']
			);

			if($_SESSION['status']=="Admin"){

				$content = $this->getView(DOCVIEW.'welcome_admin/profile.php', $p);

				$p = array(
				"content" => $content
				);
				$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

				echo $view;
			}
			else {

				$content = $this->getView(DOCVIEW.'welcome_admin/profile.php', $p);

				$p = array(
				"content" => $content
				);
				$view = $this->getView(DOCVIEW.'template/template.php', $p);

				echo $view;
			}
		}
		
		// echo json_encode($p);
	}

	public function settingProfil() {

		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> find (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if (!empty($_POST) ){
			if(isset($_POST ['nama']))
				$nama = $_POST['nama'];
			if(isset($_POST ['tanggal']))
				$tanggal = $_POST['tanggal']; 
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan']; 
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			if(isset($_POST ['jk']))
				$jk = $_POST['jk']; 
			if(isset($_POST ['alamat']))
				$alamat = $_POST['alamat']; 
			if(isset($_POST ['email']))
				$email = $_POST['email']; 
			if(isset($_POST ['pass']))
				$pass = $_POST['pass']; 
		
			$validator->addRule('nama', array('require'));
	        $validator->addRule('tanggal', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('jk', array('require'));
	        $validator->addRule('alamat', array('require'));
	        $validator->addRule('email', array('require'));
	        $validator->addRule('foto_asli', array('require'));
	        $validator->addRule('pass', array('require'));
	        $validator->setData(array(
	        	'nama' => $nama,
	            'tanggal' => $tanggal,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'jk' => $jk,
	            'alamat'=> $alamat,
	            'email' => $email,
	            'pass' =>$pass,
	        ));
	        

	        if($validator->isValid()){
	        	$tipe_gambar = array('image/jpeg','image/bmp', 'image/png');
				$foto = $_FILES['foto_asli']['name'];
				$ukuran = $_FILES['foto_asli']['size'];
				$tipe = $_FILES['foto_asli']['type'];

				if($foto!=" " && $ukuran> 0){
					if(in_array(strtolower($tipe), $tipe_gambar)){
				//move_uploaded_file($_FILES['foto']['tmp_name'], './public/fotoUser/'.$foto);	
						$temp = explode(".", $_FILES['foto_asli']['name']);
						$newfilename = round(microtime(true)) . '.' . end($temp);
						move_uploaded_file($_FILES['foto_asli']['tmp_name'], './public/fotoUser/' . $newfilename);

						if($data && $newfilename) {

	        				 $p=array( 
		        				'nama' => $nama,
					            'tanggal' => $tanggal,
					            'bulan' => $bulan,
					            'tahun' => $tahun,
					            'jk' => $jk,
					            'alamat'=> $alamat,
					            'email' => $email,
					            'pass' =>$pass,
					            'foto_asli' => $foto,
					            'foto_rename' => $newfilename,
	        	 			);

	        			} else {
	        					$p=array( 
		        				'nama' => $nama,
					            'tanggal' => $tanggal,
					            'bulan' => $bulan,
					            'tahun' => $tahun,
					            'jk' => $jk,
					            'alamat'=> $alamat,
					            'email' => $email,
					            'pass' =>$pass,
	        	 			);
	        			}
	        		}
     	
	        	 $col->update(array('_id' => new MongoId($id)), array('$set' => $p));

	        	 $col2 = $db -> auditTrail;
				
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Update Profile",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $id,
	
	        	);
	        	$col2 ->insert($p2);

	        	header( 'Location: /profil/ambildataProfil' ) ;
	        	return;
	        	}
	        }

		} //else {
			// header( 'Location: /profil/settingProfil' ) ;
	  //       	return;
		//}

		$p = array(
			"view_admin" => "",
					'nama' => $nama,
		            'tanggal' => $tanggal,
		            'bulan' => $bulan,
		            'tahun' => $tahun,
		            'jk' => $jk,
		            'alamat'=> $alamat,
		            'email' => $email,
		            'foto_asli' => $foto_asli,
		            'foto_rename' => $foto_rename,
		            'pass' =>$pass,
		);

		if($_SESSION['status']="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/profile.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		} else if($_SESSION['status']="User"){
			$content = $this->getView(DOCVIEW.'welcome_admin/profile.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template.php' , $p);

			echo $view;
		}
	}

	public function updateProfil() {

		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> find (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if (!empty($_POST) ){
			if(isset($_POST ['nama']))
				$nama = $_POST['nama'];
			if(isset($_POST ['tanggal']))
				$tanggal = $_POST['tanggal']; 
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan']; 
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			if(isset($_POST ['jk']))
				$jk = $_POST['jk']; 
			if(isset($_POST ['alamat']))
				$alamat = $_POST['alamat']; 
			if(isset($_POST ['email']))
				$email = $_POST['email'];
			if(isset($_POST ['telepon']))
				$telepon = $_POST['telepon']; 
			if(isset($_POST ['pass']))
				$pass = $_POST['pass']; 
			if(isset($_POST ['foto_asli']))
				$foto_asli = $_POST['foto_asli']; 
			if(isset($_POST ['status']))
				$status = $_POST['status']; 
		
			$validator = new Validator();
	        $validator->addRule('nama', array('require'));
	        $validator->addRule('tanggal', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('jk', array('require'));
	        $validator->addRule('alamat', array('require'));
	        $validator->addRule('email', array('require'));
	        $validator->addRule('telepon', array('numeric', 'require'));
	        $validator->addRule('pass', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('foto_asli', array('require'));
	        $validator->setData(array(
	        	'nama' => $nama,
	            'tanggal' => $tanggal,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'jk' => $jk,
	            'alamat'=> $alamat,
	            'email' => $email,
	            'telepon' =>$telepon,
	            'pass' =>$pass,
	            'foto_asli' => $foto_asli,
	            'status' => $status
	        ));
	        if($validator->isValid()){
	        	
	        	$tipe_gambar = array('image/jpeg','image/bmp', 'image/png');
				$foto = $_FILES['foto_asli']['name'];
				$ukuran = $_FILES['foto_asli']['size'];
				$tipe = $_FILES['foto_asli']['type'];

				if($foto!=" " && $ukuran> 0){
					if(in_array(strtolower($tipe), $tipe_gambar)){
				//move_uploaded_file($_FILES['foto']['tmp_name'], './public/fotoUser/'.$foto);	
						$temp = explode(".", $_FILES['foto_asli']['name']);
						$newfilename = round(microtime(true)) . '.' . end($temp);
						move_uploaded_file($_FILES['foto_asli']['tmp_name'], './public/fotoUser/' . $newfilename);
	        			
	        			if($data && $newfilename) {

	        				 $p=array( 
		        				'nama' => $nama,
					            'tanggal' => $tanggal,
					            'bulan' => $bulan,
					            'tahun' => $tahun,
					            'jk' => $jk,
					            'alamat'=> $alamat,
					            'email' => $email,
					            'telepon' => $telepon,
					           'pass' =>$pass,
					           'foto_asli' => $foto,
					            'foto_rename' => $newfilename,
					            'status' => $status
	        	 			);

	        			} else {
	        					$p=array( 
		        				'nama' => $nama,
					            'tanggal' => $tanggal,
					            'bulan' => $bulan,
					            'tahun' => $tahun,
					            'jk' => $jk,
					            'alamat'=> $alamat,
					            'email' => $email,
					            'telepon' => $telepon,
					            //'pass' =>$pass,
					            'status' => $status
	        	 			);
	        			}
	        	
	        	 		$col->update(array('_id' => new MongoId($id)), array('$set' => $p));

	        	 		$col2 = $db -> auditTrail;
					    
					    $p2=array(
					        'idUser' => $_SESSION['id'],
				        	'aksi' => "Add Aset",
				        	'tgl' => date("d-m-Y"),
				        	'jam' => date("H:i:s"),
				            'ket' => $id,
			        	);
			        	$col2 ->insert($p2);

	        			header( 'Location: /profil/ambildataProfil' ) ;
	        			return;
	       			}
	       		}
			} else {
				header( 'Location: /profil/formSetting' ) ;
		        	return;
			}

				$p = array(
					"view_admin" => "",
							'nama' => $nama,
				            'tanggal' => $tanggal,
				            'bulan' => $bulan,
				            'tahun' => $tahun,
				            'jk' => $jk,
				            'alamat'=> $alamat,
				            'email' => $email,
				            'telepon' => $telepon,
				            'foto_asli' => $foto_asli,
				            'foto_rename' => $foto_rename,
				            'pass' =>$pass,
				            'status' =>$status,
					);
					$content = $this->getView(DOCVIEW.'welcome_admin/profil/ambildataProfil.php', $p);

					$p = array(
						"content" => $content
						);
					$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

					echo $view;	
		}
	}

	// public function ambildataProfilUser() {
	/*	$id =$_SESSION['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{
			$p= array(
				"id" => $data['_id'],
				"nama" => $data['nama'],
				"tanggal" => $data['tanggal'],
				"bulan" => $data['bulan'],
				"tahun" => $data['tahun'],
				"jk" => $data['jk'],
				"alamat" => $data['alamat'],
				"email" => $data['email'],
				"pass" => $data['pass'],
				"foto" => $data['foto'],
				"status" => $data['status'],
			);

			 if($_SESSION['status']="User"){

				$content = $this->getView(DOCVIEW.'welcome/profile.php', $p);

				$p = array(
					"content" => $content
				);
				$view = $this->getView(DOCVIEW.'template/template.php', $p);

				echo $view;
			}
		}
		
		//echo json_encode($p);
	}*/

	// public function settingProfilUser() {

	// 	$id = $_GET['id'];
		
	// 	$db = Db::init();	
	// 	$col = $db -> users;		//masuk ke colom users
	// 	$data = $col -> find (	//untuk memilih satu
	// 		array(
	// 			"_id" => new mongoid($id)
	// 			)
	// 		);

	// 	if (!empty($_POST) ){
	// 		if(isset($_POST ['nama']))
	// 			$nama = $_POST['nama'];
	// 		if(isset($_POST ['tanggal']))
	// 			$tanggal = $_POST['tanggal']; 
	// 		if(isset($_POST ['bulan']))
	// 			$bulan = $_POST['bulan']; 
	// 		if(isset($_POST ['tahun']))
	// 			$tahun = $_POST['tahun']; 
	// 		if(isset($_POST ['jk']))
	// 			$jk = $_POST['jk']; 
	// 		if(isset($_POST ['alamat']))
	// 			$alamat = $_POST['alamat']; 
	// 		if(isset($_POST ['email']))
	// 			$email = $_POST['email']; 
	// 		if(isset($_POST ['pass']))
	// 			$pass = $_POST['pass']; 
		
	// 		$validator = new Validator();
	//         $validator->addRule('nama', array('require'));
	//         $validator->addRule('tanggal', array('require'));
	//         $validator->addRule('bulan', array('require'));
	//         $validator->addRule('tahun', array('require'));
	//         $validator->addRule('jk', array('require'));
	//         $validator->addRule('alamat', array('require'));
	//         $validator->addRule('email', array('require'));
	//         $validator->addRule('pass', array('require'));
	//         $validator->setData(array(
	//         	'nama' => $nama,
	//             'tanggal' => $tanggal,
	//             'bulan' => $bulan,
	//             'tahun' => $tahun,
	//             'jk' => $jk,
	//             'alamat'=> $alamat,
	//             'email' => $email,
	//             'pass' =>$pass
	//         ));
	//         if($validator->isValid()){

	// 	        if($data)

	//         	 $p=array( 
	// 	        	'nama' => $nama,
	// 	            'tanggal' => $tanggal,
	// 	            'bulan' => $bulan,
	// 	            'tahun' => $tahun,
	// 	            'jk' => $jk,
	// 	            'alamat'=> $alamat,
	// 	            'email' => $email,
	// 	            'pass' =>$pass
	//         	 );

	        	
	//         	 $col->update(array('_id' => new MongoId($id)), array('$set' => $p));

	//         	header( 'Location: /profil/ambildataProfil' ) ;
	//         	return;
	//         }

	// 	} else {
	// 		header( 'Location: /profil/settingProfilUser' ) ;
	//         	return;
	// 	}

	// 	$p = array(
	// 		"view_admin" => "",
	// 				'nama' => $nama,
	// 	            'tanggal' => $tanggal,
	// 	            'bulan' => $bulan,
	// 	            'tahun' => $tahun,
	// 	            'jk' => $jk,
	// 	            'alamat'=> $alamat,
	// 	            'email' => $email,
	// 	            'pass' =>$pass,
	// 		);
	// 	$content = $this->getView(DOCVIEW.'welcome/profile.php', $p);

	// 	$p = array(
	// 		"content" => $content
	// 		);
	// 	$view = $this->getView(DOCVIEW.'template/template.php', $p);

	// 	echo $view;
	// }


	/*public function hapusUser() {
		if(!empty($_POST)){
		$id = $_POST['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);
		if(isset($data['_id'])){
			$pathFile = './public/fotoUser/'.$data['foto'];
			if(file_exists($pathFile)){
				unlink($pathFile);
			}

			$p = array('_id' => new Mongoid($id));
			
			$col -> remove($p);

	        	$p=array(
					"hasil" => "OK"
				);
			echo json_encode($p); 
			return;
		}
		}
		$p=array(
					"hasil" => "ERROR"
				);
			echo json_encode($p);
	}*/

	public function ambildataProfil() {
		$id =$_SESSION['id'];
		
		$db = Db::init();	
		$col = $db -> users;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{
			$p= array(
				"id" => $data['_id'],
				"nama" => $data['nama'],
				"tanggal" => $data['tanggal'],
				"bulan" => $data['bulan'],
				"tahun" => $data['tahun'],
				"jk" => $data['jk'],
				"alamat" => $data['alamat'],
				"email" => $data['email'],
				"pass" => $data['pass'],
				"foto_rename" => $data['foto_rename'],
				"status" => $data['status'],
			);

			if($_SESSION['status']=='Admin'){

				$content = $this->getView(DOCVIEW.'welcome_admin/profile.php', $p);

				$p = array(
					"content" => $content
				);
				$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

				echo $view;
			}
			if($_SESSION['status']=='User'){

				$content = $this->getView(DOCVIEW.'welcome_admin/profile.php', $p);

				$p = array(
					"content" => $content
				);
				$view = $this->getView(DOCVIEW.'template/template.php', $p);

				echo $view;
			}
		}	
		//echo json_encode($p);
	}
}