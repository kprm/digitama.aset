<?php 

class audit_controller extends controller 
{



	public function listaudit() { 
		$p = array(
			"view_admin" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/auditTrail.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function ambildata(){
		$db = Db::init();
		$col = $db -> auditTrail;
		$col2 = $db -> users;
		$col3 = $db -> assets;
		$dt=$col->find();	// memilih semua == select all
		$data = array();
		foreach($dt as $dta)
		{
			$dt2=$col2->findone(array("_id" => new mongoid($dta['idUser'])));
			if($dta['ket']!="-"){
				$dt3=$col3->findone(array("_id" => new mongoid($dta['ket'])));
				$keterangan = "Aset: ".$dt3['judul'];
			} else {
				$keterangan = "-";
			}
			
			$p= array(
				"id" => trim($dta['_id']),
				"aksi" => $dta['aksi'],
				"idUser" => $dta['idUser'],
				"namaUser" => $dt2['nama'],
				"tgl" => $dta['tgl'],
				"jam" => $dta['jam'],
				"ket" => $keterangan
				
			);
			$data[]= $p;
		}

		echo json_encode($data);
	}
}
