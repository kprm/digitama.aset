<?php 

class project_controller extends controller 
{

	public function listproject() {
		
		$p = array(
			"view_admin" => "",
			
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/project/list_project.php', $p);


		$p = array(
			"content" => $content 
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function listprojectuser() {
		
		$db = Db::init();
		$col = $db -> project;
		$dt=$col->find( array())->limit(12)->sort(array("time_created" => -1));	
		
		$p = array(
			"view_admin" => "",
			"data" => $dt,
		);
		$content = $this->getView(DOCVIEW.'welcome/project.php', $p);

		$p = array( 
			"content" => $content 
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
	}

	public function formAddProject() {
		$namaProject = "";
		$tgl = ""; 
		$bulan = ""; 
		$tahun = ""; 
		// $tgls = "";
		// $bulans = "";
		// $tahuns = "";
		$status = ""; 
		$ket = ""; 
		$error = array();
		//$foto = ""; 
		if (!empty($_POST) ){
			if(isset($_POST ['namaProject']))
				$namaProject = $_POST['namaProject'];
			if(isset($_POST ['tgl']))
				$tgl = $_POST['tgl'];
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan'];
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun']; 
			// if(isset($_POST ['tgls']))
			// 	$tgls = $_POST['tgls'];
			// if(isset($_POST ['bulans']))
			// 	$bulans = $_POST['bulans'];
			// if(isset($_POST ['tahuns']))
			// 	$tahuns = $_POST['tahuns'];
			if(isset($_POST ['status']))
				$status = $_POST['status'];
			if(isset($_POST ['ket']))
				$ket = $_POST['ket']; 
			
		
			$validator = new Validator();
	        $validator->addRule('namaProject', array('require'));
	        $validator->addRule('tgl', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        // $validator->addRule('tgls', array('require'));
	        // $validator->addRule('bulans', array('require'));
	        // $validator->addRule('tahuns', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('ket', array('require'));


	       // $validator->addRule('foto', array('require'));
	        $validator->setData(array(
	        	'namaProject' => $namaProject,
	            'tgl' => $tgl,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            // 'tgls' => $tgls,
	            // 'bulans' => $bulans,
	            // 'tahuns' => $tahuns,
	            'status' => $status,
	            'ket' => $ket,
	
	        ));
	        if($validator->isValid()){
				$db =Db::init();
				$col = $db -> project;
				$p=array( 
				'namaProject' => $namaProject,
				'tgl' => $tgl,
				'bulan' => $bulan,
				'tahun' => $tahun,
				// 'tgls' => $tgls,
				//    'bulans' => $bulans,
				//    'tahuns' => $tahuns,
				'status' => $status,
				'ket' => $ket,

				);
				$col ->insert($p);
				$col2 = $db -> auditTrail;
			    $data2 = $col -> findone (	//untuk memilih satu
					array(
						'namaProject' => $namaProject,
						'status' => $status,
						)
					);
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Add Project",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $data2['_id'],
	
	        	);
	        	$col2 ->insert($p2);

				if($_SESSION['status']=="Admin"){
		        	header( 'Location: /project/listproject' ) ;
		        	return;
	        	} else if($_SESSION['status']=="User"){
		        	header( 'Location: /project/listprojectuser' ) ;
		        	return;
	        	}

	        } else {
	        	$error = $validator->getErrors();
	        }

		}

		$p = array(
			"view_admin" => "",
			'namaProject' => $namaProject,
		    'tgl' => $tgl,
		    'bulan' => $bulan,
		    'tahun' => $tahun,
		    // 'tgls' => $tgls,
		    // 'bulans' => $bulans,
		    // 'tahuns' => $tahuns,
		    'status' => $status,
		    'ket' => $ket,
		    'error' => $error
		);
		if($_SESSION['status']=="Admin"){
			$content = $this->getView(DOCVIEW.'welcome_admin/project/formAddProject.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);
		} else if($_SESSION['status']=="User"){
			$content = $this->getView(DOCVIEW.'welcome_admin/project/formAddProject.php', $p);

			$p = array(
				"content" => $content
				);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);

		}else {
			echo "error";
		}
		echo $view;
	}
	
	public function formEditProject() {
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> project;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{
			$p= array(
				"id" => $data['_id'],
				"namaProject" => $data['namaProject'],
				"tgl" => $data['tgl'],
				"bulan" => $data['bulan'],
				"tahun" => $data['tahun'],
				"status" => $data['status'],
				"ket" => $data['ket']
			
			);
			
			$content = $this->getView(DOCVIEW.'welcome_admin/project/formEditProject.php', $p);

			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

			echo $view;
		}
	}

	public function updateProject() {
 
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> project;		//masuk ke colom users
		$data = $col -> find (	//untuk memilih satu / select all
			array(
				"_id" => new mongoid($id)
				)
			);

		if (!empty($_POST) ){
			if(isset($_POST ['namaProject']))
				$namaProject = $_POST['namaProject'];
			if(isset($_POST ['tgl']))
				$tgl = $_POST['tgl']; 
			if(isset($_POST ['bulan']))
				$bulan = $_POST['bulan']; 
			if(isset($_POST ['tahun']))
				$tahun = $_POST['tahun'];
			if(isset($_POST ['status']))
				$status = $_POST['status']; 
			if(isset($_POST ['ket']))
				$ket = $_POST['ket']; 
			
		
			$validator = new Validator();
	        $validator->addRule('namaProject', array('require'));
	        $validator->addRule('tgl', array('require'));
	        $validator->addRule('bulan', array('require'));
	        $validator->addRule('tahun', array('require'));
	        $validator->addRule('status', array('require'));
	        $validator->addRule('ket', array('require'));
	        $validator->setData(array(
	        	'namaProject' => $namaProject,
	            'tgl' => $tgl,
	            'bulan' => $bulan,
	            'tahun' => $tahun,
	            'status' => $status,
	            'ket' => $ket
	        ));
	        if($validator->isValid()){

		        if($data)

		        	 $p=array( 
			        	'namaProject' => $namaProject,
			            'tgl' => $tgl,
			            'bulan' => $bulan,
			      		'tahun' => $tahun,
			      		'status' => $status,
			      		'ket' => $ket
			      
		        	 );

	        	
	        	$col->update(array('_id' => new MongoId($id)), array('$set' => $p));

	        	$col2 = $db -> auditTrail;
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "update Project",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $id,
	
	        	);
	        	$col2 ->insert($p2);
	        	header( 'Location: /project/listproject' ) ;
	        	return;
	        }

		} else {
			header( 'Location: /project/formEditProject' ) ;
	        	return;
		}

		$p = array(
			"view_admin" => "",
					'namaProject' => $namaProject,
		            'tgl' => $tgl,
		            'bulan' => $bulan,
		            'tahun' => $tahun,
		            'status' => $status,
		            'ket' => $ket
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/project/formAddProject.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function hapusProject() {
		if(!empty($_POST)){
			$id = $_POST['id'];
			
			$db = Db::init();	
			$col = $db -> project;		//masuk ke colom users
			$data = $col -> findone (	//untuk memilih satu
				array(
					"_id" => new mongoid($id)
					)
				);
			if(isset($data['_id'])){
				// $pathFile = './public/fotoUser/'.$data['foto'];
				// if(file_exists($pathFile)){
				// 	unlink($pathFile);
				// }

				$p = array('_id' => new Mongoid($id));
				
				$col -> remove($p);

		        	$p=array(
						"hasil" => "OK"
					);
				echo json_encode($p); 

				$col2 = $db -> auditTrail;
			    $data2 = $col -> findone (	//untuk memilih satu
					array(
						'namaProject' => $namaProject,
						'status' => $status,
						)
					);
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Delete Project",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $data2['_id'],
	
	        	);
	        	$col2 ->insert($p2);
				return;
			}

		}
		$p=array(
			"hasil" => "ERROR"
		);
		
		echo json_encode($p);
	}



	public function ambildataProject(){
		$db = Db::init();
		$col = $db -> project;
		$dt=$col->find();	// memilih semua == select all
		$data = array();
		foreach($dt as $dta)
		{
			
			$p= array(
				"id" => trim($dta['_id']),
				"namaProject" => $dta['namaProject'],
				"tglMulai" => $dta['tgl'].'-'.$dta['bulan'].'-'.$dta['tahun'],
				// "tglSelesai" => $dta['tgls'].'-'.$dta['bulans'].'-'.$dta['tahuns'],
				"status" => $dta['status'],
				"ket" => $dta['ket']
			);
			$data[]= $p;
		}

		echo json_encode($data);
	}

}