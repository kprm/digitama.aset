<?php 

class aset_controller extends controller 
{

	public function listaset() { 
		$p = array(
			"view_admin" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/asset/list_aset.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view; 
	}

	
	public function formAddAset() {
		$idUser = "";
		$judul = "";
		$tipe = ""; 
		$group = ""; 
		$timeUpload = "";
		$lastUpdate = "";
		$file_asli = "";
		$file_rename = ""; 
		$desk = ""; 
		$status = ""; 

	
		$error = array();
		//$foto = ""; 
		if (!empty($_POST) ){
			$idUser=$_SESSION['id'];
			// if(isset($_POST ['idUser']))
			// 	$idUser = $_POST['idUser'];
			if(isset($_POST ['judul']))
				$judul = $_POST['judul'];
			if(isset($_POST ['tipe']))
				$tipe = $_POST['tipe']; 
			if(isset($_POST ['group']))
				$group = $_POST['group']; 
			if(isset($_POST ['timeUpload']))
				$timeUpload = $_POST['timeUpload'];
			if(isset($_POST ['lastUpdate']))
				$lastUpdate = $_POST['lastUpdate']; 
			// if(isset($_POST ['file_asli']))
			// 	$file_asli = $_POST['file_asli']; 
			if(isset($_POST ['desk']))
				$desk= $_POST['desk']; 
			if(isset($_POST ['status']))
				$status= $_POST['status']; 
			
		
			$validator = new Validator();

	        //$validator->addRule('idUser', array('require'));
	        $validator->addRule('judul', array('require'));
	        $validator->addRule('tipe', array('require'));
	        $validator->addRule('group', array('require'));
	        $validator->addRule('timeUpload', array('require'));
	        $validator->addRule('lastUpdate', array('require'));
	        // $validator->addRule('file_asli', array('require'));
	        $validator->addRule('desk', array('require'));
	        $validator->addRule('status', array('require'));
	       

	        $validator->setData(array(
	        	'idUser' => $idUser,
	        	'judul' => $judul,
	        	'tipe' => $tipe,
	        	'group' => $group,
	            'timeUpload' => $timeUpload,
	            'lastUpdate' => $lastUpdate,
	            'file_asli' => $file_asli,
	            'desk' => $desk,
	            'status' => $status,

	
	        ));
	        if($validator->isValid()){


	        	$tipe_audio = array('audio/mp3', 'audio/mp4', 'audio/wav');
	        	$tipe_gambar = array('image/jpeg','image/bmp', 'image/png');
	        	$tipe_3d = array('application/zip', 'application/octet-stream');
				$file = $_FILES['file_asli']['name'];
				$ukuran = $_FILES['file_asli']['size'];
				$tipe_file = $_FILES['file_asli']['type'];

				if($file !=" " && $ukuran > 0) {
					// if((in_array(strtolower($tipe_file), $tipe_audio)) || (in_array(strtolower($tipe_file), $tipe_gambar))  || (in_array(strtolower($tipe_file), $tipe_3d))) {

						$temp = explode(".", $_FILES['file_asli']['name']);
						$newfilename = round(microtime(true)) . '.' . end($temp);
						if($tipe=="Picture"){
							move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/pict/' . $newfilename);
						} else if($tipe=="Sketch"){
							move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/sketch/' . $newfilename);
						} else if($tipe=="Audio"){
							move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/audio/' . $newfilename);
						} else if($tipe=="3D"){
							$curdir = getcwd();
							if (mkdir($curdir."/public/assets/3d/".$newfilename."", 0777)){

								move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/3d/'.$newfilename.'/' . $newfilename);
								$zip = new ZipArchive;
								$res = $zip->open('./public/assets/3d/'.$newfilename.'/'.$newfilename);
								
								if ($res == true) {
								  $zip->extractTo("./public/assets/3d/".$newfilename.'/');
								  $zip->close();
								} else {
									echo "gagal";
								}	
							} else { echo "kalo kesini gagal"; exit;}
						} else {
							echo "salah dimana?????????";
						}

					  
			        	$db =Db::init();
			        	$col = $db -> assets;
			        	$p=array( 
			        		'idUser' => $idUser,
			        		'judul' => $judul,
			        		'tipe' => $tipe,
			        		'group' => $group,
			           		'timeUpload' => $timeUpload,
			           		'time_created' => time(),
			           		'lastUpdate' => $lastUpdate,
			            	'file_asli' => $file,
			            	'file_rename' => $newfilename,
			            	'desk' => $desk,
			            	'status' => $status
			        	);
			        	$col ->insert($p);

			        	$col2 = $db -> auditTrail;
					    $data2 = $col -> findone (	//untuk memilih satu
							array(
								"idUser" => $idUser, 
								"judul" => $judul,
								"file_asli" => $file,
								"file_rename" => $newfilename
								)
							);
					    $p2=array(
					        'idUser' => $_SESSION['id'],
				        	'aksi' => "Add Aset",
				        	'tgl' => date("d-m-Y"),
				        	'jam' => date("H:i:s"),
				            'ket' => $data2['_id'],
			
			        	);
			        	$col2 ->insert($p2);
			        		
			        	header( 'Location: /myassets/coba' ) ;
			        	return;
		        	
		        	
		    	} else {
		    		echo "sssssssssssaaaaaaalllllllllaaaaaahhhhhh";
		    	}

			} else {
	        	$error = $validator->getErrors();
	        	
	        }

		}

		$p = array(
			'idUser' => $idUser,
	        'judul' => $judul,
	        'tipe' => $tipe,
	        'group' => $group,
	        'timeUpload' => $timeUpload,
	        'lastUpdate' => $lastUpdate,
	        'file_asli' => $file_asli,
	        'file_rename' => $file_rename,
	        'desk' => $desk,
	        'status' => $status,
		    'error' => $error
		);
		$content = $this->getView(DOCVIEW.'welcome/upload.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;
		exit;
	}
	
	public function formEditAset() {
		$id = $_GET['id'];
		
		$db = Db::init();	
		$col = $db -> assets;		//masuk ke colom users
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		if(isset($data['_id'])) // artinya data ada 
		{

			$p= array(
				"id" => $data['_id'],
				"idUser" => $data['idUser'],
				"judul" => $data['judul'],
				"tipe" => $data['tipe'],
				"group" => $data['group'],
				"timeUpload" => $data['timeUpload'],
				"lastUpdate" => $data['lastUpdate'],
				"file_asli" => $data['file_asli'],
				"file_rename" => $data['file_rename'],
				"desk" => $data['desk'],
				"status" => $data['status']
			
			);

			
			$content = $this->getView(DOCVIEW.'welcome/updateAset.php', $p);

			$p = array(
				"content" => $content
			);
			$view = $this->getView(DOCVIEW.'template/template.php', $p);

			echo $view;
		}
	}

	public function updateAset() {
		$id = $_GET['id'];

		$db = Db::init();	
		$col = $db -> assets;		//masuk ke colom users
		$data = $col -> find (	//untuk memilih satu / selectb all
			array(
				"_id" => new mongoid($id)
				)
			);

		if (!empty($_POST) ){
			// if(isset($_POST ['idUser']))
			// 	$idUser = $_POST['idUser'];
			if(isset($_POST ['judul']))
				$judul = $_POST['judul'];
			if(isset($_POST ['tipe']))
				$tipe = $_POST['tipe']; 
			if(isset($_POST ['group']))
				$group = $_POST['group']; 
			if(isset($_POST ['timeUpload']))
				$timeUpload = $_POST['timeUpload'];
			if(isset($_POST ['lastUpdate']))
				$lastUpdate = $_POST['lastUpdate']; 
			// if(isset($_POST ['file_asli']))
			// 	$file = $_POST['file_asli']; 
			if(isset($_POST ['desk']))
				$desk= $_POST['desk']; 
			if(isset($_POST ['status']))
				$status= $_POST['status']; 
			
		
			$validator = new Validator();
	        // $validator->addRule('idUser', array('require'));
	        $validator->addRule('judul', array('require'));
	        $validator->addRule('tipe', array('require'));
	        $validator->addRule('group', array('require'));
	        $validator->addRule('timeUpload', array('require'));
	        $validator->addRule('lastUpdate', array('require'));
	        // $validator->addRule('file', array('require'));
	        $validator->addRule('desk', array('require'));
	        $validator->addRule('status', array('require'));
	        
	        $validator->setData(array(
	        	// 'idUser' => $idUser,
	        	'judul' => $judul,
	        	'tipe' => $tipe,
	        	'group' => $group,
	            'timeUpload' => $timeUpload,
	            'lastUpdate' => $lastUpdate,
	            'file_asli' => $file_asli,
	            'desk' => $desk,
	            'status' => $status,
	        ));
	        if($validator->isValid()){
	        	$tipe_audio = array('audio/mp3', 'audio/mp4', 'audio/wav');
	        	$tipe_gambar = array('image/jpeg','image/bmp', 'image/png');
	        	$tipe_3d = array('application/zip', 'application/octet-stream');
				$file = $_FILES['file_asli']['name'];
				$ukuran = $_FILES['file_asli']['size'];
				$tipe_file = $_FILES['file_asli']['type'];

				if($file !=" " && $ukuran > 0) {
					// if((in_array(strtolower($tipe_file), $tipe_audio)) || (in_array(strtolower($tipe_file), $tipe_gambar))  || (in_array(strtolower($tipe_file), $tipe_3d))) {

						$temp = explode(".", $_FILES['file_asli']['name']);
						$newfilename = round(microtime(true)) . '.' . end($temp);
						if($tipe=="Picture"){
							move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/pict/' . $newfilename);
						} else if($tipe=="Sketch"){
							move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/sketch/' . $newfilename);
						} else if($tipe=="Audio"){
							move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/audio/' . $newfilename);
						} else if($tipe=="3D"){
							$curdir = getcwd();
							if (mkdir($curdir."/public/assets/3d/".$newfilename."", 0777)){

								move_uploaded_file($_FILES['file_asli']['tmp_name'], './public/assets/3d/'.$newfilename.'/' . $newfilename);
								$zip = new ZipArchive;
								$res = $zip->open('./public/assets/3d/'.$newfilename.'/'.$newfilename);
								
								if ($res == true) {
								  $zip->extractTo("./public/assets/3d/".$newfilename.'/');
								  $zip->close();
								} else {
									echo "gagal";
								}	
							} else { echo "kalo kesini gagal"; exit;}
						} else {
							echo "salah dimana?????????";
						}	 
	        
		        		if($data){

	        	 			$p=array( 
		        				// 'idUser' => $idUser,
	        					'judul' => $judul,
	        					'tipe' => $tipe,
	        					'group' => $group,
	           	 				'timeUpload' => $timeUpload,
	           	 				'time_created' => time(),
	           	 				'lastUpdate' => $lastUpdate,
	           					'file_asli' => $file,
	           					'file_rename' => $newfilename,
	            				'desk' => $desk,
	            				'status' => $status
		      
	        	 			);
	        			}
	        	
	        	 		$col->update(array('_id' => new MongoId($id)), array('$set' => $p));


	        	 		$col2 = $db -> auditTrail;
					    $data2 = $col -> findone (	//untuk memilih satu
							array(
								"idUser" => $idUser, 
								"judul" => $judul,
								"file_asli" => $file,
								"file_rename" => $newfilename
								)
							);
					    $p2=array(
					        'idUser' => $_SESSION['id'],
				        	'aksi' => "Update Aset",
				        	'tgl' => date("d-m-Y"),
				        	'jam' => date("H:i:s"),
				            'ket' => $data2['_id'],

			        	);
			        	$col2 ->insert($p2);

	        			header( 'Location: /myassets/coba' ) ;
	        			return;
	        		//}
	        	}
	        }

		} else {
			header( 'Location: /aset/formEditAset' ) ;
	        	return;
		}

		$p = array(
			"view_admin" => "",
					'idUser' => $idUser,
	        		'judul' => $judul,
	        		'tipe' => $tipe,
	        		'group' => $group,
	           	 	'timeUpload' => $timeUpload,
	            	'lastUpdate' => $lastUpdate,
	            	'file_asli' => $file_asli,
	            	"file_rename" => $file_rename,
	            	'desk' => $desk,
	            	'status' => $status,
			);
		$content = $this->getView(DOCVIEW.'welcome/updateAset.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template.php', $p);

		echo $view;

		
	}

	public function hapusAset() {
		if(!empty($_POST)){
			$id = $_POST['id'];
			
			$db = Db::init();	
			$col = $db -> assets;		//masuk ke colom users
			$data = $col -> findone (	//untuk memilih satu
				array(
					"_id" => new mongoid($id)
					)
				);
			if(isset($data['_id'])){
				$pathFile = './public/assets/'.$data['file_rename'];
				if(file_exists($pathFile)){
					unlink($pathFile);
				}

				$p = array('_id' => new Mongoid($id));
				$col -> remove($p);
		        $p=array(
					"hasil" => "OK"
				);
				echo json_encode($p); 

				$col2 = $db -> auditTrail;
			    $p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Delete Aset",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $id,
	
	        	);
	        	$col2 ->insert($p2);
				return;
			}
		}
		$p=array(
			"hasil" => "ERROR"
		);
		echo json_encode($p);
	}

	


	public function ambildataAset(){

		$nmgrup = ""; 
		$pemilik = "";
		$jmldownload="";

		$db = Db::init();
		$col = $db -> assets;
		$col2 = $db -> group;
		$col3 = $db -> users;
		$col4 = $db -> download;

		$dt=$col->find();	// memilih semua == select all
		$data = array();

		foreach($dt as $dta)
		{		
			$dt2 = $col2 -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($dta['group'])
				)
			);

			$dt3 = $col3 -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($dta['idUser'])
				)
			);

			$dt4 = $col4->count(
			array(
				"idAset" => new mongoid(trim($dta['_id']))
				)
			);

			if(isset($dt2['_id']))
			{
				$nmgrup = $dt2['namaGrup'];
				$pemilik = $dt3['nama'];
				// $jmldownload = count($dt4);
				
			
				$p= array(
					"id" => trim($dta['_id']),
					"idUser" => $dta['idUser'],
					"pemilik" => $pemilik,
					"judul" => $dta['judul'],
					"tipe" => $dta['tipe'],
					"group" => $dta['group'],
					"namaGrup" => $nmgrup,
					"timeUpload" => $dta['timeUpload'],
					"lastUpdate" => $dta['lastUpdate'],
					"file_rename" => $dta['tipe'].'+'.$dta['file_rename'],
					"file_asli" => $dta['file_asli'],
					"desk" => $dta['desk'], 
					"status" => $dta['status'], 
					"jmldownload" => $dt4
					
				);
			}
			$data[]= $p;	
		} 
		echo json_encode($data);
	}

	public function detail() {
		$id =$_GET['id'];
		// $idAset=$_POST['idAset'];
		$nmgrup = ""; 
		$idCommenter="";
		$cmnt="";
		$pemilik ="";
		$commenter="";
		$db = Db::init();	

		$col = $db -> assets;		//masuk ke colom users
		$col2 = $db -> group;
		$col3 = $db -> comments;
		$col4 = $db -> users;
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($id)
				)
			);

		// $dt3=$col3->find( array("idAset" => new mongoid($data['_id'])))->sort(array("time_created" => 1));
		$dt3=$col3->find( array("idAset" => $id));

		if(isset($data['_id'])) // artinya data ada 
		{
			$dt2 = $col2 -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($data['group'])
				)
			);

			$dt4 = $col4 -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($data['idUser'])
				)
			);

			if(isset($dt2['_id']))
			{
				$nmgrup = $dt2['namaGrup'];
				$pemilik = $dt4['nama'];
				
			$p= array(
				"id" => trim($data['_id']),
				"idUser" => $data['idUser'],
				"pemilik" => $pemilik,
				"judul" => $data['judul'],
				"tipe" => $data['tipe'],
				"group" => $data['group'],
				"namaGrup" => $nmgrup,
				"timeUpload" => $data['timeUpload'],
				"lastUpdate" => $data['lastUpdate'],
				"file_rename" => $data['file_rename'],
				"file_asli" => $data['file_asli'],
				"desk" => $data['desk'],
				"status" => $data['status'],
				"dataa" => $dt3,
			);

			
				if($_SESSION['status']=="Admin"){
					if(($data['tipe']== "Sketch") || ($data['tipe']== "Picture")){
						$content = $this->getView(DOCVIEW.'welcome_admin/detail.php', $p);
					} else if($data['tipe']== "Audio"){
						$content = $this->getView(DOCVIEW.'welcome_admin/detailmp3.php', $p);
					}

					$p = array(
						"content" => $content
					);
					$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

					echo $view;
				} else {
					if(($data['tipe']== "Sketch") || ($data['tipe']== "Picture")){
						$content = $this->getView(DOCVIEW.'welcome_admin/detail.php', $p);
					} else if($data['tipe']== "Audio"){
						$content = $this->getView(DOCVIEW.'welcome_admin/detailmp3.php', $p);
					}
					$p = array(
						"content" => $content
					);
					$view = $this->getView(DOCVIEW.'template/template.php', $p);

					echo $view;
				}
			}
		}
		//echo json_encode($p);
	}

	public function download() {
		$idAset = $_GET['id'];
		$idPemilik="";
		$idDownloader=$_SESSION['id'];

		$db = Db::init();	
		$col = $db -> assets;	
		$col2 = $db -> download;	//masuk ke colom users
		$col3 = $db -> auditTrail;
		$data = $col -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($idAset)
				)
			);
		if(isset($data['_id'])) // artinya data ada 
		{
			$file_name=$data['file_rename'];
			if ($data['tipe']=="Picture"){
				$folder='./public/assets/pict/'.$file_name;
			}else if ($data['tipe']=="Sketch"){
				$folder='./public/assets/sketch/'.$file_name;
			} else if ($data['tipe']=="Audio"){
				$folder='./public/assets/audio/'.$file_name;
			} else if ($data['tipe']=="3D"){
				$folder='./public/assets/3d/'.$file_name;
			}

			if(file_exists($folder)){
				//$file_url = 'http://www.myremoteserver.com/' . $file_name;
				header('Content-Type: application/octet-stream');
	 			header("Content-Transfer-Encoding: Binary"); 
				header('Content-Length: ' . filesize($folder));
				header("Content-disposition: attachment; filename=\"".$data['file_asli']."\""); 
				readfile($folder);
				
	        	return;
	        	exit;

	        	$p=array( 
	        		'idAset' => $data['_id'],
	        		'idPemilik' => $data['idUser'], 
	        		'idDownloader' => $idDownloader,
	        		'tanggal' => date('d-m-Y H:i')
	        	);
	        	$col2 ->insert($p);

	        	$p2=array(
			        'idUser' => $_SESSION['id'],
		        	'aksi' => "Download Aset",
		        	'tgl' => date("d-m-Y"),
		        	'jam' => date("H:i:s"),
		            'ket' => $data['_id']
	
	        	);
	        	$col3 ->insert($p2);
			}
		}
	}


	public function listdownload() { 
		$p = array(
			"view_admin" => ""
			);
		$content = $this->getView(DOCVIEW.'welcome_admin/asset/list_download.php', $p);

		$p = array(
			"content" => $content
			);
		$view = $this->getView(DOCVIEW.'template/template_admin.php', $p);

		echo $view;
	}

	public function ambildatadownload(){

		$judul = ""; 
		$pemilik = "";
		$pendownload="";

		$db = Db::init();
		$col = $db -> download;
		$col2 = $db -> users;
		$col3 = $db -> assets;

		$dt=$col->find();	// memilih semua == select all
		$data = array();

		foreach($dt as $dta)
		{		
			$dt2 = $col2 -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($dta['idPemilik'])
				)
			);

			$dt3 = $col2 -> findone (	//untuk memilih satu
			array(
				"_id" => new mongoid($dta['idDownloader'])
				)
			);

			$dt4 = $col3 -> findone(
			array(
				"_id" => new mongoid($dta['idAset'])
				)
			);

			if(isset($dt2['_id'])){

				$judul = $dt4['judul']; 
				$pemilik = $dt2['nama'];
				$pendownload=$dt3['nama'];

				
				$p= array(
					"id" => trim($dta['_id']),
					"judul" => $judul,
					"idPemilik" => $dta['idPemilik'],
					"pemilik" => $pemilik,
					"idDownloader" => $dta['idDownloader'],
					"pendownload" => $pendownload,
					"tanggal" => $dta['tanggal']
					

				);
				
				$data[]= $p;	
			}
		}
		echo json_encode($data);
	}
}