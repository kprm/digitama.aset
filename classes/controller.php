<?php
class controller {

	public function __construct ()
	{
		if(!isset($_SESSION['status']))
		{
			$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
			if(($path != "/login/index") && ($path != "/login/callback"))
			{
				header( 'Location: /login/index' ) ;
				exit;
			}
		}
	}

	protected function getView($filename, $variable)
	{

		extract($variable);
		ob_start();  
		(include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;

	}

}