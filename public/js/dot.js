
$(function() {
	$( "body" ).delegate( ".hapus", "click", function(e) {
		e.preventDefault();
		strid = $(this).attr('dataid');
		
		bootbox.confirm("Kamu akan menghapus data ini ?", function(result) {
			if(result){
				//alert("Hallo " + strid);
				 $.post( '/user/hapusUser', { id: strid, 
				  })
					 .done(function( data ) {
					 var obj = JSON.parse(data);
					 if(obj.hasil=="OK")
					 {
					 	window.location="http://assets.digitama.com/user/listuser";
					 }
 					
				 
				});
			}
		}); 

	});

	$( "body" ).delegate( ".hapusAset", "click", function(e) {
		e.preventDefault();
		strid = $(this).attr('dataid');
		
		bootbox.confirm("Kamu akan menghapus data ini ?", function(result) {
			if(result){
				//alert("Hallo " + strid);
				 $.post( '/aset/hapusAset', { id: strid, 
				  })
					 .done(function( data ) {
					 var obj = JSON.parse(data);
					 if(obj.hasil=="OK")
					 {
					 	window.location="http://assets.digitama.com/aset/listaset";
					 }
 					
				 
				});
			}
		}); 

	});

	$( "body" ).delegate( ".hapusGroup", "click", function(e) {
		e.preventDefault();
		strid = $(this).attr('dataid');
		
		bootbox.confirm("Kamu akan menghapus data ini ?", function(result) {
			if(result){
				//alert("Hallo " + strid);
				 $.post( '/group/hapusGroup', { id: strid, 
				  })
					 .done(function( data ) {
					 var obj = JSON.parse(data);
					 if(obj.hasil=="OK")
					 {
					 	window.location="http://assets.digitama.com/group/listgroup";
					 }
 					
				 
				});
			}
		}); 

	});
});

